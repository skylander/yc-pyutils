# This project has been DEPRECATED, and will be superseded by [yc-utils](https://bitbucket.org/skylander/yc-utils).

# Documentation for yc-pyutils

A collection of handy utility scripts for NLP with Python (mainly data processing related).

For more information, refer to the documentation [http://skylander.bitbucket.org/yc-pyutils](http://skylander.bitbucket.org/yc-pyutils)

# Changelog

DEPRECATED

## Version 1.0 (development)
- Retructured `ycutils/` folder. NLP related modules go into `nlp/` folder and `tsvio` went into `io/` folder.
- Fixed documentation to reflect the update.

### 1/2/2013
- Added `tokenizer` module which uses a new paradigm for tokenization.

## Version 0.2 (development)

### 4/6/2013
- Reworked tokenization module. Many bugs found.
- Added scripts `tokenize-docs.py` and `build-vocab.py`.
- Added `filter_rare_terms` method to `BOW` class.

## Version 0.1

- Initial version. Very much work in progress.



# License

yc-pyutils is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

yc-pyutils is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with yc-pyutils.  If not, see [http://www.gnu.org/licenses/](http://www.gnu.org/licenses).

