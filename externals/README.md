External libraries used in yc-pyutils
=====================================

splitta
-------
Using version 1.03, downloaded on 9/12/2012 from [http://code.google.com/p/splitta/](http://code.google.com/p/splitta/).

Unidecode
---------
Using version 0.04.12, downloaded on 4/5/2013 from [https://pypi.python.org/pypi/Unidecode](https://pypi.python.org/pypi/Unidecode).
