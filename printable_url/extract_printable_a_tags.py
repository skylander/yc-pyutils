import sys, argparse, re
import ycutils.urls

"""
This script extracts all anchor tags from a set of downloaded HTML files.

A list of tab separated URL and filenames (where the URL is downloaded to) is read from the STDIN, and the full ``<a>...</a>`` tags are printed to STDOUT.
"""

parser = argparse.ArgumentParser(description='Extract <a> tags from HTML files containing the word \'print\' and output them. Reads from STDIN a list of tab separated url and filename.')
A = parser.parse_args()

__RE_PRINT = re.compile(r'.*\bprint\b.*', re.I)
def is_print_link(tag):
  if tag.name != 'a': return False
  
  href = tag.get('href')
  
  if not href: return False
  if tag.get('href').startswith('javascript'): return False
  if tag.get('href').startswith('#'): return False

  if __RE_PRINT.match(str(tag)): return True

  return False
#end def

for i, line in enumerate(sys.stdin):
  (url, filename) = line.strip().split('\t')
  # (url, mime_type, mod_date, status, filename) = line.strip().split('\t')

  print >>sys.stderr, '{}: {}'.format(i, url)

  # if status != 'webpage-ok': continue
  # if mime_type != 'text/html': continue

  with open(filename) as f:
    content = f.read()

  soup = bs4.BeautifulSoup(content, "lxml")

  L = set()
  for a_tag in soup.find_all(is_print_link):
    L.add(str(a_tag).replace('\n', '').replace('\r', ''))

  if L:
    for t in L:
      print '+\t{}\t{}'.format(url, t)
  #end if
#end for
