import sys, argparse, re, os, urlparse, collections, random, cPickle
import bs4, ycutils.urls
import numpy as np
import sklearn.linear_model, sklearn.preprocessing

"""
Given a file of annotated instances of anchor links, this script trains a logistic regression model and saves it as a file, which can be used with :class:`ycutils.urls.printable.PrintableURL` class.

The annotated instances file is a tab separated file. 
Each line is an instance containing a label (``+`` or ``-``), the URL and the full anchor tag all in one line.A

Below is an example::
  + \t http://www.nytimes.com/2011/12/25/opinion/sunday/arab-spring.html \t <a href="/2011/12/25/opinion/sunday/arab-spring.html?_r=0&amp;pagewanted=print">Print</a>

A set of annotated instances can be found in ``annotated-print-tags.txt`` located in the same directory as this script.
"""

MIN_FEATURE_COUNT = 5

parser = argparse.ArgumentParser(description='Train logistic regression classifier for classifying printable links.')
parser.add_argument('annotated_file', type=file, help='Input file where annotated instances are.')
parser.add_argument('-t', '--training-proportion', type=float, default=0.7, metavar='p', help='Amount of data to use as training data.')
parser.add_argument('-i', '--test-model', type=file, default=None, metavar='<model>', help='Test performance of model on training and test instances.')
parser.add_argument('-o', '--save-model', type=argparse.FileType('w'), default=None, metavar='<model>', help='Save model in <model>.')

A = parser.parse_args()

def unigram_features(prefix, s):
  feats = collections.Counter()

  if not s: return feats
  if isinstance(s, list): s = ''.join(s)

  toks = re.split(r'[\W\s]+', s.lower())

  for t in toks:
    if not t: continue

    feats[prefix + t] = 1
  #end for

  return feats
#end def

def extract_features(instance_string):
  (label, url, tag_str) = instance_string.split('\t', 2)

  soup = bs4.BeautifulSoup(tag_str)
  a_tag = soup.find('a')

  text_features = unigram_features('text_', a_tag.get_text())
  url_features = unigram_features('url_', url)
  href_features = unigram_features('href_', a_tag['href'])

  followed_url = ycutils.urls.follow_url(url, a_tag['href'])
  url_toks = set(filter(lambda x: x, re.split(r'[\W\s0-9]+', url.lower())))
  followed_toks = set(filter(lambda x: x, re.split(r'[\W\s0-9]+', followed_url.lower())))
  diff_toks = url_toks ^ followed_toks

  features = collections.Counter()
  features += url_features
  features += href_features
  features += unigram_features('title_', a_tag.get('title'))
  features += unigram_features('rel_', a_tag.get('rel'))
  features += unigram_features('class_', a_tag.get('class'))
  features += unigram_features('id_', a_tag.get('id'))
  features += text_features
  features += collections.Counter(['diff_' + t for t in diff_toks])
  features['meta_textlen'] = sum(text_features.values())
  features['meta_hreflen'] = sum(href_features.values())
  features['meta_difflen'] = len(diff_toks)

  return label, url, features
#end def

def evaluate_model(model, scaler, feature_index, instances):
  N = len(instances)
  p = len(feature_index)

  test_matrix = np.zeros((N, p), dtype=np.float)
  y_test = np.zeros((N, 1), dtype=np.int)

  for i, line in enumerate(instances):
    label, url, features = extract_features(line)
    for feat_name, feat_val in features.iteritems():
      if feat_name in feature_index: test_matrix[i, feature_index[feat_name]] = feat_val
    if label == '+': y_test[i] = 1
  #end for
  pred = model.predict(scaler.transform(test_matrix))

  fn = 0; fp = 0
  tn = 0; tp = 0
  i = 0
  for pr, gold in zip(pred, y_test):
    if pr == 0 and gold == 0: tn += 1
    elif pr == 0 and gold == 1: fn += 1
    elif pr == 1 and gold == 0: fp += 1
    else: tp += 1

    if pr != gold and gold == 0:
      (label, url, tag_str) = instances[i].split('\t', 2)
      print gold, url, tag_str
    #end if
    i += 1
  #end for

  return tp, tn, fp, fn
#end def

all_feats = collections.Counter()
instances = []
for line in A.annotated_file:
  line = line.strip()
  if line: instances.append(line)
#end for

random.shuffle(instances)
if A.training_proportion > 1.0 or A.training_proportion <= 0: parser.error('Training proportion must be between 0 and 1.')
training_count = int(A.training_proportion * len(instances))
train_instances = instances[:training_count]
test_instances = instances[training_count:]

if A.test_model:
  scaler = cPickle.load(A.test_model)
  lr_model = cPickle.load(A.test_model)

  feature_names = cPickle.load(A.test_model)
  feature_index = {}
  for i, feat_name in enumerate(feature_names): feature_index[feat_name] = i

else:
  print >>sys.stderr, 'Extracting features for train instances...'
  train_features = []
  feature_totals = collections.Counter()
  for i, line in enumerate(train_instances, start=1):
    (label, url, features) = extract_features(line)
    train_features.append((label, url, features))
    feature_totals += features
  #end for

  # compile list of features
  feature_index = {}
  feature_names = []
  i = 0
  for feat_name in sorted(feature_totals.keys()):
    if feature_totals[feat_name] > MIN_FEATURE_COUNT:
      feature_index[feat_name] = i
      feature_names.append(feat_name)
      i += 1
    #end if
  #end for
  del feature_totals

  N_train = len(train_features)
  p = len(feature_names)

  train_matrix = np.zeros((N_train, p), dtype=np.float)
  y_train = np.zeros((N_train, 1), dtype=np.int)
  for i, (label, url, features) in enumerate(train_features):
    for feat_name, feat_val in features.iteritems():
      if feat_name in feature_index: train_matrix[i, feature_index[feat_name]] = feat_val
    if label == '+': y_train[i] = 1
  #end for
  print >>sys.stderr, 'There are {} positive examples and {} negative examples.'.format(np.count_nonzero(y_train), N_train - np.count_nonzero(y_train))
  print >>sys.stderr, 'There are {} features.'.format(p)

  scaler = sklearn.preprocessing.Scaler()
  X = scaler.fit_transform(train_matrix)

  print >>sys.stderr, 'Training logistic regression model...'
  lr_model = sklearn.linear_model.LogisticRegression(penalty='l1', C=1, intercept_scaling=1e-5, tol=1e-5, class_weight={0: 0.3, 1: 1})
  lr_model.fit(X, y_train)

  model_coeffs = lr_model.coef_.ravel()
  sparsity_l1_LR = np.mean(model_coeffs == 0) * 100.0

  print >>sys.stderr,''
  print >>sys.stderr, 'Model sparsity (L1 penalty) is {:.2f}.'.format(sparsity_l1_LR)
  print >>sys.stderr, 'Top 30 features are:'
  feat_coeff = sorted(zip(model_coeffs, feature_names), reverse=True, key=lambda (x, y): abs(x))
  for (feat_coeff, feat_name) in feat_coeff[:30]:
    print >>sys.stderr, '{:8.5f}\t{}'.format(feat_coeff, feat_name)
  print >>sys.stderr,''

  if A.save_model:
    cPickle.dump(scaler, A.save_model)
    cPickle.dump(lr_model, A.save_model)
    cPickle.dump(feature_names, A.save_model)

    print >>sys.stderr, 'Saved model in <{}>.'.format(A.save_model.name)
  #end if
#end if

N_train = len(train_instances)
if N_train > 0:
  print >>sys.stderr, ''
  print >>sys.stderr, 'Evaluating model on train set ({} instances)...'.format(N_train)
  (tp, tn, fp, fn) = evaluate_model(lr_model, scaler, feature_index, train_instances)
  print >>sys.stderr, 'Model accuracy: {:.2f}%'.format(float(tp+tn) / N_train * 100.0)
  print >>sys.stderr, 'False positive: {}'.format(fp)
  print >>sys.stderr, 'False negaive : {}'.format(fn)
#end if

N_test = len(test_instances)
if N_test > 0:
  print >>sys.stderr, ''
  print >>sys.stderr, 'Evaluating model on test set ({} instances)...'.format(N_test)
  (tp, tn, fp, fn) = evaluate_model(lr_model, scaler, feature_index, test_instances)
  print >>sys.stderr, 'Model accuracy: {:.2f}%'.format(float(tp+tn) / N_test * 100.0)
  print >>sys.stderr, 'False positive: {}'.format(fp)
  print >>sys.stderr, 'False negaive : {}'.format(fn)
#end if
