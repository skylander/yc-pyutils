#!/bin/env python

import argparse, itertools, re, operator
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.mlab as mlab

parser = argparse.ArgumentParser(description='Plot log-likelihoods from log-likelihood files.')
parser.add_argument('ll_files', type=file, nargs='+', help='A list of files containing log-likelihoods. File format is iterN<TAB>ll_N on each line (or simply just likelihoods on each line).')
A = parser.parse_args()

likelihoods = {}
max_iters = 0
for f in A.ll_files:
  likelihoods[f.name] = {'iter': [], 'll': []}

  for i, line in enumerate(f, start=1):
    line = line.strip()
    if not line or line.startswith('#'): continue

    fields = re.split(r'[ |\t]+', line)
    if len(fields) == 0: continue
    if len(fields) == 1:
      likelihoods[f.name]['iter'].append(i)
      likelihoods[f.name]['ll'].append(float(fields[0]))
    else:
      likelihoods[f.name]['iter'].append(int(fields[0]))
      likelihoods[f.name]['ll'].append(float(fields[1]))
    #end if
  #end for

  max_iters = max(max_iters, max(likelihoods[f.name]['iter']))
#end for

marker = itertools.cycle(('+', '.', 'o', '*', 'x', ',', 's', 'D', 'v', '^'))

plt.hold(True)
for ll_name in sorted(likelihoods.iterkeys()):
  ll_info = likelihoods[ll_name]

  if len(ll_info['iter']) <= 20: plt.plot(ll_info['iter'], ll_info['ll'], ls='-', linewidth=0.5, marker=marker.next(), label=ll_name)
  else: plt.plot(ll_info['iter'], ll_info['ll'], ls='-', linewidth=0.5, label=ll_name)
#end for

if max_iters <= 20: plt.xticks(np.arange(1, max_iters+1))

# plt.axis('tight')
plt.title('Log-likelihoods against iterations')
plt.xlabel('Iterations')
plt.ylabel('Log-likelihoods')
plt.legend(loc='lower right')

plt.show()
