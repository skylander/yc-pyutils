#!/usr/bin/env python

import argparse, sys, codecs
import ycutils.nlp.corpus

parser = argparse.ArgumentParser(description='Prune vocabulary size. If freq/docfreq are < 1.0, the lower/top proportion will be used instead.')
parser.add_argument('vocab_file', type=str, metavar='<vocab_file>', help='Vocab file to prune.')
parser.add_argument('save_as', type=str, metavar='<pruned_vocab>', help='File path to save pruned vocabulary file.')

parser.add_argument('-f', '--min-freq', type=float, default=None, help='Minimum vocabulary frequency.')
parser.add_argument('-F', '--max-freq', type=float, default=None, help='Maximum vocabulary frequency.')
parser.add_argument('-d', '--min-docfreq', type=float, default=None, help='Minimum document frequency.')
parser.add_argument('-D', '--max-docfreq', type=float, default=None, help='Maximum document frequency.')
parser.add_argument('-i', '--min-idf', type=float, default=None, help='Minimum inverse document frequency.')
parser.add_argument('-I', '--max-idf', type=float, default=None, help='Maximum inverse document frequency.')
parser.add_argument('-x', '--exclude-terms', type=file, metavar='<exclude_file>', help='A file containing list of terms to exclude from vocabulary.')

A = parser.parse_args()

print >>sys.stderr, 'Loading vocabulary file...'
vocab = ycutils.nlp.corpus.CorpusVocabulary(from_filename=A.vocab_file)

excludes = []
if A.exclude_terms:
  f = codecs.getreader('utf-8')(A.exclude_terms)
  excludes = filter(lambda line: line, map(lambda line: line.strip(), f))
#end if

freqs = []
docfreqs = []
idfs = []
for (w, (freq, df, idf)) in vocab.iteritems():
  freqs.append(freq)
  docfreqs.append(df)
  idfs.append(idf)
#end for

freqs.sort()
docfreqs.sort()
idfs.sort()
L = len(vocab)

if A.min_freq and A.min_freq < 1.0: A.min_freq = freqs[int(A.min_freq * L)]
if A.max_freq and A.max_freq < 1.0: A.max_freq = freqs[int(A.max_freq * L)]
if A.min_docfreq and A.min_docfreq < 1.0: A.min_docfreq = docfreqs[int(A.min_docfreq * L)]
if A.max_docfreq and A.max_docfreq < 1.0: A.max_docfreq = docfreqs[int(A.max_docfreq * L)]
# if A.min_idf and A.min_idf < 1.0: A.min_idf = freqs[int(A.min_idf * L)]
# if A.max_idf and A.max_idf < 1.0: A.max_idf = freqs[int(A.max_idf * L)]

print >>sys.stderr, 'Minimums={}'.format((A.min_freq, A.min_docfreq, A.min_idf))
print >>sys.stderr, 'Maximums={}'.format((A.max_freq, A.max_docfreq, A.max_idf))

print >>sys.stderr, 'Filtering vocabulary items...'
vocab.filter(minimums=(A.min_freq, A.min_docfreq, A.min_idf), maximums=(A.max_freq, A.max_docfreq, A.max_idf), remove_terms=excludes)

print >>sys.stderr, 'Saving pruned vocabulary...'
with codecs.open(A.save_as, 'w', 'utf-8') as vocab_file:
  vocab.to_file(vocab_file)
print >>sys.stderr, 'Saved {} word types to {}.'.format(len(vocab), A.save_as)

