#!/usr/bin/env python

import argparse, sys, codecs
import ycutils.nlp.corpus
import numpy as np
import matplotlib.pyplot as plt

parser = argparse.ArgumentParser(description='View statistic about corpus vocabulary.')
parser.add_argument('vocab_file', type=str, help='Vocab file.')
A = parser.parse_args()

print >>sys.stderr, 'Loading vocabulary file...'
vocab = ycutils.nlp.corpus.CorpusVocabulary(from_filename=A.vocab_file)

freqs = []
dfreqs = []
for (w, (f, df, idf)) in vocab.iteritems():
  dfreqs.append(df)
  freqs.append(f)
#end for

freqs = np.array(freqs)
dfreqs = np.array(dfreqs)
type_count = len(vocab) - 1

print 'Token count: {}'.format(sum(freqs))
print 'Type count : {}'.format(type_count)
print
print 'Average type frequency         : {:.2f} +/- {:.2f}'.format(np.mean(freqs), np.std(freqs))
print 'Average type document frequency: {:.2f} +/- {:.2f}'.format(np.mean(dfreqs), np.std(dfreqs))

print
freqs = np.sort(freqs)[::-1]
for p in [0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1.0]:
  c = np.sum(freqs) * p
  for i, x in enumerate(freqs, start=1):
    c -= x
    if c <= 0:
      print '{:3d}% of tokens are contained in the {:5d} most frequent types.'.format(int(p * 100), i)
      break
    #end if
  #end for
#end for
