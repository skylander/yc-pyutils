======================================
Documentation for yc-pyutils |version|
======================================
This documentation describes the handy python modules that I use frequently in the course of my research in Natural Language Processing.

.. toctree::
  :hidden:

  nlp/bagofwords
  nlp/bigvocab
  nlp/bleu
  nlp/corpus
  nlp/tfidf
  nlp/tokenize
  nlp/tokenizer
  nlp/tokenizer-taggers
  nlp/tokenizer-normalizers
  io
  io/tsvio
  urls
  urls/webpages
  urls/printable
  urls/googlebooks
  urls/wikipedia
  urls/youtube
  misc
  strings

  scripts/plot-likelihood
  scripts/tokenize-docs
  scripts/build-vocab
  scripts/prune-vocab
  scripts/vocab-stats


===========
NLP modules
===========
There are several sub modules for handling NLP text processing operations.

====================================      ==============================
Modules                                   Description
====================================      ==============================
:doc:`bagofwords <nlp/bagofwords>`        Classes and methods that are useful when handling bags of words data structure.
:doc:`bigvocab <nlp/bigvocab>`            Classes and methods for handling vocabulary of corpora with millions of tokens. (**unreliable, untested!**)
:doc:`bleu <nlp/bleu>`                    Methods for computing the BLEU precision/recall from two given list of tokens.
:doc:`corpus <nlp/corpus>`                Classes and methods that are useful when handling corpora.
:doc:`tfidf <nlp/tfidf>`                  Classes and methods for computing TF-IDF values.
:doc:`tokenize <nlp/tokenize>`            Methods to split a piece of text into individual sentences and into individual words. (**deprecated**)
:doc:`tokenizer <nlp/tokenizer>`          Classes and methods for tokenizing text. This is a more robust and better version over the previous :mod:`ycutils.nlp.tokenize` module.
====================================      ==============================


==========
IO modules
==========
These modules deal with reading and writing of files, in various formats, and/or handling files in general.

====================================      ==============================
Modules                                   Description
====================================      ==============================
:doc:`io <io>`                            Miscelleanous IO methods.
:doc:`tsvio <io/tsvio>`                   :class:`ycutils.io.tsvio.TSVFile` class handles reading/writing to tab separated values files.
====================================      ==============================


============
URLs modules
============
This module contains classes and methods that are useful when trying to download data from the Web.

*TODO: Standardize download mechanisms for all these below and more robust JSON handling.*

=====================================      ==============================
Modules                                    Description
=====================================      ==============================
:doc:`webpages <urls/webpages>`            Download webpages by calling Wget with :mod:`subprocess`. (*TODO: a cURL implementation and more robust support of proxies, Tor, etc.*)
:doc:`wikipedia <urls/wikipedia>`          Methods for downloading `Wikipedia <http://www.wikipedia.org>`_ articles.
:doc:`googlebooks <urls/googlebooks>`      Downloading descriptions of books from `Google books <http://books.google.com>`_.
:doc:`youtube <urls/youtube>`              Methods for downloading `Youtube <http://www.youtube.com>`_ video descriptions.
:doc:`printable <urls/printable>`          Methods for identifying "printable" links and downloading printable versions of webpages.
=====================================      ==============================


=============
String module
=============
Currently a single module file containing miscellaneous convenience string methods.

:doc:`strings <strings>`


=====================
Miscellaneous modules
=====================
Modules and methods which do not have a category of its own.

====================================      ==============================
Modules                                   Description
====================================      ==============================
:doc:`misc <misc>`                        Miscellaneous functions that don't really fall under any category.
====================================      ==============================


==============
Useful scripts
==============
Here are some useful scripts for performing common NLP preprocessing tasks.
These script depends heavily on the above modules and classes.
They can be found in ``scripts/`` directory of the package.

===================================================  ==============================
Scripts                                              Description
===================================================  ==============================
:doc:`plot-likelihood.py <scripts/plot-likelihood>`  This script displays a graphical plot of likelihood against iterations.
:doc:`tokenize-docs.py <scripts/tokenize-docs>`      This script tokenizes large collections of text using :mod:`ycutils.tokenize`.
:doc:`build-vocab.py <scripts/build-vocab>`          This script builds a vocabulary of the corpus using :mod:`ycutils.corpus` methods.
:doc:`prune-vocab.py <scripts/prune-vocab>`          This script prunes the vocabulary file according to criterion specified on the command line.
:doc:`vocab-stats.py <scripts/vocab-stats>`          This script displays statistics about the given :class:`ycutils.corpus.CorpusVocabulary` file.
===================================================  ==============================


=======
License
=======
yc-pyutils is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

yc-pyutils is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with yc-pyutils.  If not, see `http://www.gnu.org/licenses/ <http://www.gnu.org/licenses/>`_.

