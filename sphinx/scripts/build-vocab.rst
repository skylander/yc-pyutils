===================================
Build vocabulary (*build-vocab.py*)
===================================

Use this script to build a vocabulary of the corpus using :mod:`ycutils.nlp.corpus` methods.

.. code-block:: none

    usage: build-vocab.py [-h] [-f <file> [<file> ...]] [-l <files_list>]
                          [-d <input_dir>] [--ignore-comments]
                          [--multi-file-format] [--strip-unicode]
                          [--filter-stopwords [<stopword_file>]] [--ignore-rare N]
                          [--ngrams [n [n ...]]] [--ngram-separator <separator>]
                          [--ngram-stopwords [<stopwords> [<stopwords> ...]]]
                          [--stemming | --plus-stemming] [--no-tfidf]
                          [--idf-smoothing <lambda>]
                          <vocab_file>



:doc:`build-vocab.py <build-vocab>` allows you to specify input and output files in several manners.
By default, it reads from ``STDIN``.
All I/O is done in ``utf-8`` encoding.

  * ``<vocab_file>`` - a required argument that denote where we are saving the generated vocabulary file.

Specifying input

  * ``-f <file> [<file> ...], --files <file> [<file> ...]`` - Input is a list of files.
  * ``-f <files_list>, --fileslist <files_list>`` - Input is a file containing a list of filenames.
  * ``-d <input_dir>, --input-dir <input_dir>`` - Recursively searches ``<input_dir>`` for files and extract terms from them.
  * ``--ignore-comments`` - Ignore lines that start with '#' (default: false).
  * ``--multi-file-format`` - Input is in the multi file format (for example, that produced by :doc:`tokenize-docs.py <tokenize-docs>`). Treats each empty line followed line starting with ``#`` as a new document.

Text options

  * ``--strip-unicode`` - Strip input text of unicode and force everything to be ASCII. This is quite aggressive and uses the Unidecode library.
  * ``--filter-stopwords [<stopword_file>]`` - Filter stopwords from input texts. You can specify a text file (``utf-8`` encoding) containing a list of stopwords, one on each line.
  * ``--ignore-rare N`` - If N > 1, filter words that appear < N times in each document. If N < 1, filters away the smallest N portion of types. Uses :meth:`ycutils.nlp.bagofwords.BOW.filter_rare_terms`.

N-gram options

  * ``--ngrams [n [n ...]]`` - Generate n-gram terms (default: unigram only). You can specify multiple n-gram sizes here.
  * ``--ngram-separator <separator>`` - Separator string for n-grams (default: ``_``).
  * ``--ngram-stopwords [<stopwords> [<stopwords> ...]]`` - Additional stopword (on top of those specified by ``--filter-stopwords``) to ignore when generating n-grams, for instance ``__PUNCT__`` (default: ``__PUNCT__``).

Stemming options

  * ``--stemming`` - Apply Porter stemming to tokens.
  * ``--plus-stemming`` - Apply Porter stemming to tokens and include them in addition to non-stemmed terms.

TF-IDF options

  * ``--no-tfidf`` - Just build list of terms and not compute TF-IDF information. Vocabulary file will be a list of types, one on each own line.
  * ``idf-smoothing <lambda>`` - Laplace smoothing factor for computing IDF.
