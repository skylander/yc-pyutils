======================================
Plot likelihood (*plot-likelihood.py*)
======================================

This script displays a graphical plot of likelihood against iterations.
This script uses (`matplotlib.pyplot <http://matplotlib.org/api/pyplot_api.html>_`) for plotting.

.. code-block:: none

    usage: plot-likelihood.py [-h] ll_files [ll_files ...]


It reads likelihood information of an iterative procedure from text file of the following formats:

  * File containing lines of the form ``<iteration><TAB><likelihood>``, or
  * File containing lines of the form ``<likelihood>``.

A line starting with ``#`` is automatically ignored.

The positional arguments are:

  * ``ll_files`` - Each ``ll_file`` denote a likelihood file and will be plotted as a line on the graph. You can plot as many lines as you like on the plot.

Example
-------

.. figure:: plot-likelihood-eg.png
  :height: 400
  :align: center
  
  Log likelihood plots for the example ``plot-likelihood.py no_unigrams.reltypes-30x*.ll``.