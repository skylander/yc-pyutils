===================================
Prune vocabulary (*prune-vocab.py*)
===================================

This script prunes the vocabulary file according to criterion specified on the command line.

.. code-block:: none

    usage: prune-vocab.py [-h] [-f MIN_FREQ] [-F MAX_FREQ] [-d MIN_DOCFREQ]
                          [-D MAX_DOCFREQ] [-i MIN_IDF] [-I MAX_IDF]
                          [-x <exclude_file>]
                          <vocab_file> <pruned_vocab>

The positional arguments are:

  * ``<vocab_file>`` - Vocabulary file to prune (in the :class:`ycutils.nlp.corpus.CorpusVocabulary` format).
  * ``<pruned_vocab>`` - File path to save pruned vocabulary file.

Optional arguments are:

  * ``-f MIN_FREQ, --min-freq MIN_FREQ`` and ``-F MAX_FREQ, --max-freq MAX_FREQ`` - Specifies the minimum and maximum term frequency in pruned vocabulary file.
  * ``-d MIN_DOCFREQ, --min-docfreq MIN_DOCFREQ`` and ``-D MAX_DOCFREQ, --max-docfreq MAX_DOCFREQ`` - Specifies the minimum and maximum document frequency in pruned vocabulary file.
  * ``-i MIN_IDF, --min-idf MIN_IDF`` and ``-I MAX_IDF, --max-idf MAX_IDF`` - Specifies the minimum and maximum inverse document frequency in pruned vocabulary file.
  * ``-x <exclude_file>, --exclude-terms <exclude_file>`` - A file containing list of terms to exclude from vocabulary.

If either of ``MIN_FREQ, MAX_FREQ, MIN_DOCFREQ`` or ``MAX_DOCFREQ`` is smaller than 1.0, it will be interpreted as the smallest/largest percentage of the vocabulary instead.
