=======================================
Tokenize documents (*tokenize-docs.py*)
=======================================

Use this script tokenize large collections of text using :mod:`ycutils.nlp.tokenize`.
For details about its workings and argument settings, you can refer to the methods :meth:`ycutils.nlp.tokenize.words`.

.. code-block:: none

    usage: tokenize-docs.py [-h] [-f <file> [<file> ...]] [-l <files_list>]
                            [-L <file_pairs>] [-D <dir> <dir>] [-d <input_dir>]
                            [-P] [-S]
                            [-T [{separator,punctuation,symbol,phone,time,date,url,email,number,money} [{separator,punctuation,symbol,phone,time,date,url,email,number,money} ...]]]
                            [-N [{phone,time,date,url,email,number,money,punctuation,symbol,consecutive,case} [{phone,time,date,url,email,number,money,punctuation,symbol,consecutive,case} ...]]]
                            [--hyphens {keep,del,split}]
                            [--clitics {keep,del,split}]
                            [--neg-clitics {keep,del,split}]
                            [--no-normalize-clitics-quote] [--stemming]
                            [--strip-unicode]
                            [--filter-stopwords [<stopword_file>]]
                            [--ngrams [n [n ...]]] [--ngram-separator <separator>]
                            [--ngram-stopwords [<stopwords> [<stopwords> ...]]]


:doc:`tokenize-docs.py <tokenize-docs>` allows you to specify input and output files in several manners.
By default, it reads from ``STDIN`` and outputs to ``STDOUT``.
All I/O is done in ``utf-8`` encoding.

Specifying input/output options

  * ``-f <file> [<file> ...], --files <file> [<file> ...]`` - Input is a list of files. Tokenized files will be printed to ``STDOUT``.
  * ``-l <files_list>, --fileslist <files_list>`` - Input is a file containing a list of filenames. Tokenized files will be printed to ``STDOUT``.
  * ``-L <file_pairs>, --filepairs <file_pairs>`` - Input is a file containing pairs of input/output files on each line, separated by a single ``<TAB>`` character.
  * ``-D <dir> <dir>, --io-dir <dir> <dir>`` - Input is a pair of directories. The files/directory structure in ``<input_dir>`` will be mirrored in ``<output_dir>``. Non existing directories will be created.
  * ``-d <input_dir>, --input-dir <input_dir>`` - Input is a directory and tokenized files will be printed to ``STDOUT``. This is recursive.

When multiple files are being printed to ``STDOUT``, each file on the output is marked with a line ``# <filename>``. The end of a file is marked with an empty line.

Sentence splitting options

  * ``-P, --no-split-paragraph`` - Split text by paragraphs first (i.e splitting on instances of ``\n\n``) (default: split paragraphs).
  * ``-S, --no-split-sentence`` - Split text by sentences (using Splitta library) (default: split sentences).

Tag/normalization options

  * ``--ignore-tags [{separator,punctuation,symbol,phone,time,date,url,email,number,money} [{separator,punctuation,symbol,phone,time,date,url,email,number,money} ...]]`` - Specify categories of tags to ignore (default: ``separator``). See to :ref:`here <tag-list>` for more information.
  * ``--normalize-tags [{phone,time,date,url,email,number,money,punctuation,symbol,consecutive,case} [{phone,time,date,url,email,number,money,punctuation,symbol,consecutive,case} ...]]`` - Specify tagged categories to normalize. Defaults to everything: ``[phone, time, date, url, email, number, money, punctuation, symbol, consecutive, case]``. See to :ref:`here <normalization-options>` for more information.
  * ``--hyphens {keep,del,split}`` - Specify what to do with hyphenated words: ``keep``, ``del`` or ``split`` (default: ``split``).
  * ``--clitics {keep,del,split}`` - Specify what to do with non-negative clitics: ``keep``, ``del`` or ``split`` (default: ``del``).
  * ``--neg-clitics {keep,del,split}`` - Specify what to do with negative clitics: ``keep``, ``del`` or ``split`` (default: ``keep``).
  * ``--no-normalize-clitics-quote`` - Standardize the single quote used in clitics to the ``ASCII`` version ``'`` (default: yes).

  * ``--stemming`` - Apply Porter stemming to tokens.
  * ``--strip-unicode`` - Strip input text of unicode and force everything to be ASCII. This is quite aggressive and uses the Unidecode library.
  * ``--filter-stopwords [<stopword_file>]`` - Filter stopwords from input texts. You can specify a text file (``utf-8`` encoding) containing a list of stopwords, one on each line.

N-gram options

  * ``--ngrams [n [n ...]]`` - Generate n-gram terms (default: unigram only). You can specify multiple n-gram sizes here.
  * ``--ngram-separator <separator>`` - Separator string for n-grams (default: ``_``).
  * ``--ngram-stopwords [<stopwords> [<stopwords> ...]]`` - Additional stopword (on top of those specified by ``--filter-stopwords``) to ignore when generating n-grams, for instance ``__PUNCT__`` (default: ``__PUNCT__``).

