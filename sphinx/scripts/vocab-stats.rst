========================================
Vocabulary statistics (*vocab-stats.py*)
========================================

This script displays statistics about the given :class:`ycutils.nlp.corpus.CorpusVocabulary` file.
It displays information such as the token counts, type counts and average tokens/document frequency in the corpus.

.. code-block:: none

   usage: vocab-stats.py [-h] vocab_file

The positional arguments are:
  * ``vocab_file`` - Vocabulary file to display statistics for (in the :class:`ycutils.nlp.corpus.CorpusVocabulary` format).

Example
=======

.. code-block:: none

    $ ./vocab-stats.py vocabulary.txt
    Loading vocabulary file...
    Token count: 860433.0
    Type count : 29950

    Average type frequency         : 28.73 +/- 116.50
    Average type document frequency: 13.58 +/- 9.99

     10% of tokens are contained in the    45 most frequent types.
     20% of tokens are contained in the   292 most frequent types.
     30% of tokens are contained in the   898 most frequent types.
     40% of tokens are contained in the  2024 most frequent types.
     50% of tokens are contained in the  3840 most frequent types.
     60% of tokens are contained in the  6542 most frequent types.
     70% of tokens are contained in the 10309 most frequent types.
     80% of tokens are contained in the 15350 most frequent types.
     90% of tokens are contained in the 21834 most frequent types.
    100% of tokens are contained in the 29950 most frequent types.
