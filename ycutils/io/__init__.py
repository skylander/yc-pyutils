"""
**************
*io* module
**************

This module contains convenience IO methods.

Methods
=======
.. autofunction:: ycutils.io.directory_arg
.. autofunction:: ycutils.io.directory_output_arg
.. autoclass:: ycutils.io.codecs_file
.. autofunction:: ycutils.io.listdir
.. autofunction:: ycutils.io.read
.. autofunction:: ycutils.io.write
"""

import codecs
import os


def directory_arg(s):
  """Checks if argument `s` is a valid directory. Often used as `type` argument to :mod:`argparse`.

  :param s: string containing a path to a directory.
  :raises IOError: when `s` is not a valid directory.
  :returns: `s` on success.
  """

  if not os.path.isdir(s): raise IOError('`{}` is not a valid directory.'.format(s))

  return s
#end def


def directory_output_arg(s):
  """For `type` argument to :mod:`argparse`, where the directory is for output. A new directory (including necessary parent directories) will be created if it does not exist.

  :param s: string containing a path to a directory.
  :raises IOError: when `s` is not a valid directory.
  :returns: `s` on success.
  """

  if not os.path.isdir(s): os.makedirs(s)
  if not os.path.isdir(s): raise IOError('`{}` is not a valid directory.'.format(s))

  return s
#end def


class codecs_file:
  """
  Convenience object for use with the `type` argument in :mod:`argparse`, where the argument is a file which is encoded in some :mod:`codecs` supported format.

  :param string mode: opens a file using the specified mode.
  :param string encoding: encoding to read/write file with.
  """

  def __init__(self, mode='r', encoding='utf-8'):
    self.mode_ = mode
    self.encoding_ = encoding
  #end def

  def __call__(self, s):
    """Opens file path :attr:`s` using :mod:`codecs` module.

    :param string s: path to file.
    :param string encoding: encoding to open file with.
    :returns: file-like object returned by :meth:`codecs.open`.
    """

    return codecs.open(s, self.mode_, self.encoding_)
  #end def
#end class


def listdir(path, include_path=True):
  """
  Convenience wrapper to :meth:`os.listdir` which allows one to include the path to file for entries returned.

  :param string path: path to directory for listing.
  :param include_path: each file entry will include `path`. If set to `False`, it will be the same as using :meth:`os.listdir` directly.
  :returns: a generator for files in directory `path`.
  """

  for fname in os.listdir(path):
    if include_path: fname = os.path.join(path, fname)
    yield fname
  #end for
#end def


def read(filename=None, fileobj=None, encoding=None):
  """Read entire contents of file from `filename` or `fileobj` into memory.

  :param string filename: string containing path to filename to read from.
  :param fileobj: file-like object to read from.
  :type fileobj: file-like object
  :param encoding: if encoding is specified, :mod:`codecs` is used to open the file with the specified `encoding`.
  """

  if filename:
    f = codecs.open(filename, 'rb', encoding) if encoding else open(filename, 'rb')
    content = f.read()
    f.close()
  elif fileobj: content = fileobj.read()
  else: raise ValueError('Need to specify filename or file-like object to read from.')
  #end if

  return content
#end def


def write(content, filename=None, fileobj=None, encoding=None):
  """Write entire contents into `filename` or `fileobj`.

  :param string filename: string containing path to filename to write to.
  :param fileobj: file-like object to write to.
  :type fileobj: file-like object
  :param encoding: if encoding is specified, :mod:`codecs` is used to open the file with the specified `encoding`.
  """

  if filename:
    f = codecs.open(filename, 'wb', encoding) if encoding else open(filename, 'wb')
    f.write(content)
    f.close()
  elif fileobj: fileobj.write(content)
  else: raise ValueError('Need to specify filename or file-like object to write to.')
  #end if

  return
#end def
