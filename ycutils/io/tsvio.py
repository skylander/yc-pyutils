"""
**************
*tsvio* module
**************

This module contains the :class:`TSVFile` class which handles reading/writing to tab separated values files.

..  code-block:: python

    import ycutils.io.tsvio

    T = ycutils.io.tsvio.TSVFile()
    with open('test.txt', 'w') as f:
      T.writeline(f, 'hello world!', comment=True)
      T.writeline(f, ['a', 'b', 'c'])
      T.writeline(f, (1, 2, 3))
      T.writeline(f, (4, 5, 6))
      T.writeline(f, (7, 8, 9))
    #end with

    with open('test.txt', 'r') as f:
      for r in T.readlines(f):
        print r

    # ['a', 'b', 'c']
    # ['1', '2', '3']
    # ['4', '5', '6']
    # ['7', '8', '9']

*TSVFile* class
================
.. autoclass:: ycutils.io.tsvio.TSVFile
  :members:
"""

class TSVFile:
  """This class is used for reading/writing to files formatted with tab separation.

  There are limited formatting enhancements available, such as comment lines and named column headers.
  If column headers are specified, functions like :meth:`.readline` and :meth:`.readlines` will return a dictionary instead of a list.

  :param column_headers: specifies the name of the columns in our TSV data
  :param column_count: specifies the number of columns in our TSV data
  :param comment_char: treat line as comment if the first character of the line is this character. Set to `None` to disable comments.
  :param separator: separator character to use.
  :param skip_empty: whether to ignore empty lines or return them.
  """

  column_headers = None
  """The column headers of the TSV file."""

  __comment_char = None
  __separator = '\t'
  __skip_empty = True
  __column_count = None

  def __init__(self, column_headers=None, column_count=None, comment_char='#', separator='\t', skip_empty=True):
    """Instantiate the :class:`TSVFile` object with given settings."""

    self.column_headers = column_headers
    self.__column_count = column_count - 1 if column_count else -1
    self.__comment_char = comment_char
    self.__separator = separator
    self.__skip_empty = skip_empty
  #end def

  def readline(self, f):
    """Reads a line from given file descriptor.

    If the line is empty and :attr:`skip_empty` is set to True, or comments are enabled, it will move on to the next available line.

    :param f: file descriptor to read from.
    :returns: a list/dict of data read from a line from :attr:`f`."""

    line = None
    while True:
      line = f.readline()

      if not line: return None

      line = line.strip('\n')
      if self.__skip_empty and not line: continue
      if self.__comment_char and line.startswith(self.__comment_char): continue

      break # exit loop when we finally have a good string
    #end while

    return self.parseline(line)
  #end def

  def readlines(self, f):
    """Reads from given file descriptor until EOF.

    :param f: file descriptor to read from.
    :returns: a generator object for each line of useful (e.g not empty or comment) data in :attr:`f`"""

    for line in f:
      line = line.strip('\n')

      if self.__skip_empty and not line: continue
      if self.__comment_char and line.startswith(self.__comment_char): continue

      yield self.parseline(line)
    #end for
  #end def

  def writeline(self, f, row, comment=False):
    """Writes to given file descriptor a line of TSV.

    :param f: file descriptor to write to.
    :param row: a list of fields that we want tab separated.
    :param comment: whether this is a comment line. Comment lines are not tab separated."""

    if comment: f.write('#'+str(row))
    else:
      if isinstance(row, dict) and self.column_headers: f.write(self.__separator.join([str(row[header]) for header in self.column_headers]))
      else: f.write(self.__separator.join(map(lambda x: unicode(x), row)))
    #end if

    f.write('\n')
  #end def

  def parseline(self, line):
    """Takes a line of TSV'ed string and make a nice list/dict out of it.

    :param line: the line to process
    :returns: either a list of field values or dictionary with column headers as keys.
    :rtype: :class:`list` or :class:`dict`"""

    fields = line.split(self.__separator, self.__column_count)

    if self.column_headers:
      row = {}
      for i, header in enumerate(self.column_headers):
        try:
          row[header] = fields[i]
        except IndexError:
          raise IndexError('There are not enough values to unpack in line [{}]'.format(line))
      #end for

      return row
    #end if

    return fields
  #end def
#end class
