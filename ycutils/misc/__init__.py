"""
*************
*misc* module
*************

This module contains miscellaneous functions that don't really fall under any category.

Methods
=======
.. autofunction:: progress_iter
.. autofunction:: progress
"""
import sys


def progress_iter(iter, interval=(1, 5), wait_char='.', write_to=sys.stderr, done=True):
  """ Useful for displaying a progress update.

  :param iter: an iterable (will do progress update after every `yield` statement)
  :type iter: Iterable
  :param interval: how often to display a numerical update instead of :attr:`wait_char`.
  :param write_to: specifies which stream to write updates to. Defaults to :attr:`sys.stderr`.
  :returns: a :attr:`wait_char` (defaults to ``.``) or a number (every :attr:`interval` updates).
  :rtype: string.
  """

  for idx, o in enumerate(iter, start=1):
    yield o
    if idx % interval[1] == 0 or idx <= 1: write_to.write(str(idx))
    if idx % interval[0] == 0: write_to.write(wait_char)
    write_to.flush()
  #end for

  write_to.write('..done!\n')
  write_to.flush()
#end def


def progress(i, interval=(1, 5), wait_char='.', write_to=sys.stderr):
  """ Useful for displaying a progress update.

  :param i: the current index
  :type i: int
  :param interval: how often to display a numerical update instead of :attr:`wait_char`.
  :param write_to: specifies which stream to write updates to. Defaults to :attr:`sys.stderr`.
  :returns: a :attr:`wait_char` (defaults to ``.``) or a number (every :attr:`interval` updates).
  :rtype: string.
  """

  if i % interval[1] == 0 or i <= 1: write_to.write(str(i))
  if i % interval[0] == 0: write_to.write(wait_char)
#end def
