"""
******************
*fancy_tbl* module
******************

This module contains the implementation of a class for displaying tabular data in ASCII style.

*FancyASCIITable* class
=======================
.. autoclass:: ycutils.fancy_tbl.FancyASCIITable
  :members:
"""

import cStringIO, sys

class FancyASCIITable:
  __widths = []
  __fixed_width = 0
  __default_justification = 'l'
  __colcount = 0
  __table_data = []
  __row_justifications = []
  __lpadding = 0
  __rpadding = 0
  __separators = None

  def __init__(self, widths=[], fixed_width=0, default_justification='l', left_padding=1, right_padding=1, separators=('|', '-', '+')):
    """Defines the dimensions of the table (no. of columns and width). By default, all columns are variable sized.

    :param widths: a list of the widths of each column of the table.
    :param fixed_width: use a fixed width for every column of the table.

    :param default_justification: the default justification of cell contents: ``l`` (left), ``r`` (right), or ``c`` (center).

    :param left_padding: number of spaces to pad left of cell contents.
    :param right_padding: number of spaces to pad right of cell contents.

    :param separators: a 3-tuple consisting of separator characters between columns, rows and the intersections respectively.
    """

    self.__widths = widths
    self.__fixed_width = 0
    self.__colcount = 0
    self.__table_data = [[]]
    self.__row_justifications = [[]]
    self.__default_justification = default_justification
    self.__lpadding = left_padding
    self.__rpadding = right_padding
    self.__separators = separators
  #end def

  def add_row(self, items, justifications=[]):
    """Add a row of data to the table. This function will convert each item into a string using the Python builtin :meth:`str` method.

    :param items: list of items to build row.
    :returns: table's current number of columns.
    :rtype: int
    """

    if len(items) > self.__colcount:
      self.__colcount = len(items)

    self.__table_data.append(map(str, items))
    self.__row_justifications.append(justifications)

    return self.__colcount
  #end def

  def add_row_border(self):
    """Add a row border to the table. By default, the first and last row of the table will be a border, so there is no need to manually add one.

    :returns: table's current number of columns.
    :rtype: int"""

    self.__table_data.append([])
    self.__row_justifications.append([])

    return self.__colcount
  #end def

  def __str__(self):
    """Returns a string representation of this fancy table."""

    if not self.__colcount:
      return ''

    output = cStringIO.StringIO()

    widths = [0 for i in xrange(self.__colcount)]
    for j, w in enumerate(self.__widths):
      widths[j] = w

    for row in self.__table_data:
      for j, col in enumerate(row):
        widths[j] = max([widths[j], len(col), self.__fixed_width])
    #end for
    
    end_border = [([], [])] if self.__table_data[-1] else []
    

    for row, just in zip(self.__table_data, self.__row_justifications) + end_border:
      output.write(self.__separators[2 if not row else 0])

      for j in xrange(self.__colcount):
        cell_just = self.__default_justification
        if j < len(just): cell_just = just[j]

        if not row:
          output.write(''.ljust(widths[j] + self.__lpadding + self.__rpadding, self.__separators[1]))
          output.write(self.__separators[2])

        elif row:
          output.write(''.ljust(self.__lpadding))

          if j < len(row):
            if cell_just == 'l': output.write(row[j].ljust(widths[j]))
            if cell_just == 'c': output.write(row[j].center(widths[j]))
            if cell_just == 'r': output.write(row[j].rjust(widths[j]))
          else:
            output.write(''.ljust(widths[j]))
          #end if

          output.write(''.ljust(self.__rpadding))
          output.write(self.__separators[0])
        #end if
      #end for

      output.write('\n')
    #end for

    return output.getvalue()
  #end def

  def display(self, output=sys.stdout):
    """Display table in fancy ASCII form to file object given by :attr:`output`.

    :param output: file object to display fancy table.
    """

    output.write(self.__str__())
  #end def
#end class
