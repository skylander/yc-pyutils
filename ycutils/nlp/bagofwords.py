"""
*******************
*bagofwords* module
*******************

This module contains classes and methods that are useful when handling bags of words data structure.

Example:

..  code-block:: python

    import math
    import ycutils.nlp.bagofwords

    # BOW class example
    x = ycutils.nlp.bagofwords.BOW(['hello', 'hello', 'world'])
    y = ycutils.nlp.bagofwords.BOW(['bye', 'bye', 'world'])

    print x
    print y
    assert(x * y == 1)
    assert(x.l2_norm() == math.sqrt(5))

    # Document class example
    a = ycutils.nlp.bagofwords.Document('a', ['hello', 'hello', 'world'])
    b = ycutils.nlp.bagofwords.Document('b', ['bye', 'bye', 'world'])

    print a
    print b

    # getting cosine similarity between a BOW and Document object
    assert(abs(ycutils.nlp.bagofwords.cosine_similarity(a, x) - 1.0) < 1e-8)

Methods
=======
.. autofunction:: cosine_similarity
.. autofunction:: random_title

*BOW* class
===========
.. autoclass:: ycutils.nlp.bagofwords.BOW
  :members:
  :special-members:  __mul__

*Document* class
================
.. autoclass:: ycutils.nlp.bagofwords.Document
  :members:
  :special-members:
"""
import collections, math, sys, random, cPickle


class BOW(collections.Counter):
  """The bag of words class, which is based on the :class:`Counter` class in :mod:`collections`.

  This class provides the data structures and methods to store, retrieve, output, etc, of bags of words.

  :param tokens: iterable list of tokensto add to the bag of word.
  :param wc_string: initialize bag of words with a word:count formatted string."""

  def __init__(self, tokens=None, wc_string=None):
    if tokens: self.add_tokens(tokens)
    if wc_string: self.add_wc_string(wc_string)
  #end def

  def __str__(self):
    """The string representation.

    See :func:`to_wc_string`."""

    return self.to_wc_string()
  #end def

  def __mul__(self, other):
    """Multiplies two :class:`BOW`.

    :param other: the other BOW object to find the dot product with.
    :returns: the dot product of this class with ``other``.

    See :func:`dot_product`."""

    return self.dot_product(other)
  #end def

  def __iadd__(self, other):
    """Adds two :class:`BOW` in place.

    :param other: the other BOW object to add."""

    for w, c in other.iteritems(): self[w] += c

    return self
  #end def

  def filter_rare_terms(self, limit):
    """
    Filter rare terms from the :class:`BOW`.
    If :attr:`limit` > 1, terms that appear < :attr:`limit` will be removed.
    If 0 < :attr:`limit` < 1, the rarest :attr:`limit` * 100 percent of the :class:`BOW` (in terms of counts) will be removed.

    :param limit: the cutoff for removing rare terms.

    :returns: number of items removed.
    """

    count = 0
    if limit > 1.0:
      for w, c in self.items():
        if c < limit:
          count += 1
          del self[w]
        #end if
      #end for

    elif limit < 1.0 and limit >= 0:
      n = int(math.floor(limit * len(self)))
      for w, c in self.most_common()[:-n:-1]:
        count += 1
        del self[w]
      #end for
    #end if

    return count
  #end def

  def add_tokens(self, tokens):
    """Adds a list of tokenized words to the bag of words collections.

    :param tokens: the list of tokens to add to our :class:`BOW`. Should be an iterable."""

    self.update(tokens)
  #end def

  def add_wc_string(self, s):
    """Adds words and their counts from a ``word:count`` formatted string.

    :param s: a string of the form in ``word:count`` format."""
    for wc_str in s.split():
      w, c = wc_str.split(':')
      c = float(c)
      if c == 0: continue
      self[w] += float(c)
    #end for
  #end def

  def to_wc_string(self):
    """Format the :class:`BOW` object in a ``word:count`` formatted string which looks like ``word1:count1 word2:count2 ...``.

    :returns: the formatted string."""
    return ' '.join([u'{}:{}'.format(w, c) for w, c in self.iteritems()])
  #end def

  def to_sentence(self, sort=False):
    """Returns a sentence which is equivalent (assuming non-negative integer counts) in content to the :class:`BOW` object.

    :returns: a sentence string."""
    words = []
    for w, c in self.iteritems(): words += [w] * int(c)

    if sort: words.sort()

    return ' '.join(words)
  #end def

  def dot_product(self, other):
    """Iterates through words in the counter and multiplies counts for the same words together.

    :param other: the other BOW object to find the dot product with.
    :returns: dot product of this object with ``other``"""

    if len(self) == 0 or len(other) == 0: return 0.0

    dot_prod = 0.0
    for w, c in self.iteritems(): dot_prod += c * other[w]

    return dot_prod
  #end def

  def l2_norm(self):
    """:returns: the L2-norm of the bag of words vector."""
    return math.sqrt(sum([c * c for c in self.itervalues()]))
  #end def

  def l1_norm(self):
    """:returns: the L1-norm of the bag of words vector."""
    return sum(self.itervalues())
  #end def

  def normalize(self, sum_to=1.0):
    """Normalizes the counts of words, such that they sum up to :attr:`sum_to`.

    :param sum_to: total count of words after normalizing."""

    c = self.l1_norm() / float(sum_to)
    for w in self.iterkeys(): self[w] /= c
  #end def
#end class

class Document(BOW):
  """A document data structure. Basically a :class:`BOW` object with a :attr:`title` attribute.

  :param title: title of this document, should be unique.
  :param tokens: iterable list of tokens in the document.
  :param bow: bag of words object to 'convert' to a document object.
  :param wc_string: initialize document with a word:count formatted string.
  """

  title = None
  """The title of this document."""

  def __init__(self, title='', tokens=[], bow=None, wc_string=None):
    if bow: self.update(bow)
    if tokens: self.add_tokens(tokens)
    if wc_string: self.add_wc_string(wc_string)

    self.title = str(title) if title else random_title()
  #end def

  def __str__(self):
    """The string representation.

    See :func:`to_wc_string`."""

    return self.to_wc_string()
  #end def

  def to_wc_string(self):
    """Format the :class:`Document` object in a ``word:count`` formatted string which looks like ``title<tab>word1:count1 word2:count2``.

    :returns: the formatted string."""

    return u'{}\t{}'.format(self.title, ' '.join([u'{}:{}'.format(w, c) for w, c in self.iteritems()]))
  #end def

  def add_wc_string(self, s):
    """Adds tokens and their counts from a ``word:count`` document formatted string.

    :param s: a string of the form in ``word:count`` format."""

    try:
      title, wc_str = s.split('\t', 1)
    except ValueError: raise ValueError('Tab separator for title and word:counts not found.')

    self.title = title
    super(Document, self).add_wc_string(wc_str)
  #end def

  def bow(self):
    """:returns: the :class:`BOW` object of this document (i.e without the title)"""

    return super(Document, self)
  #end def
#end class

def cosine_similarity(bow1, bow2):
  """Calculates the cosine similarity of two bag of words vectors (or any :class:`Counter`-like object really).

  :param bow1: the bag of words vector.
  :param bow2: the bag of words vector.
  :returns: the cosine similarity."""
  if len(bow1) == 0 or len(bow2) == 0: return 0.0

  dot_prod = 0.0
  bow1_norm = 0.0
  bow2_norm = 0.0

  for w, c in bow1.iteritems():
    dot_prod += c * bow2[w]
    bow1_norm += c * c

  if dot_prod == 0.0: return 0.0

  for c in bow2.itervalues(): bow2_norm += c * c

  return dot_prod / math.sqrt(bow1_norm * bow2_norm)
#end def

def random_title():
  """:returns: return a randomly generate 16 character hexadecimal string."""

  return ''.join(random.choice('0123456789abcdef') for i in xrange(16))
#end def
