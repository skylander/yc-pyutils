"""
*****************
*bigvocab* module
*****************

This module contains classes and methods for handling corpora with millions of tokens.

..  code-block:: python

    import ycutils.nlp.bagofwords
    import ycutils.nlp.bigvocab
    import ycutils.nlp.corpus

    # Vocabulary class (with create_token=True)
    BV = ycutils.bigvocab.VocabularyMap(corpus_vocabulary=ycutils.corpus.CorpusVocabulary(C, unknown_token=None), create_token=True, unknown_token=None)

    print 'BV.get_indexes([\'new\', \'world\']) = {}'.format(BV.get_indexes(['new', 'world']))
    BV.to_file(sys.stdout)
    print 'BV.get_tokens([5, 3]) = {}'.format(BV.get_tokens([5, 3]))

    # index   tokens
    # -----   ------
    # 0       this
    # 1       world
    # 2       is
    # 3       bye
    # 4       hello
    # 5       new
    # BV.get_indexes(['new', 'world']) = [5, 1]
    # BV.get_tokens([5, 3]) = ['new', 'bye']

    print BV.keys_to_indexes(x)
    print BV.keys_to_tokens(BV.keys_to_indexes(y))
    # {1: 1, 4: 2}
    # {'world': 1, 'bye': 2}

    # BigBOW can be used seamlessly within a Document
    Bx = ycutils.nlp.bagofwords.Document('Bx', ycutils.nlp.bigvocab.BigBOW(BV, bow=x))
    By = ycutils.nlp.bagofwords.Document('By', ycutils.nlp.bigvocab.BigBOW(BV, bow=y))
    print Bx # Bx      1:1 4:2
    print By # By      1:1 3:2

    # Vocabulary class (with create_token=False)
    del BV
    BV = ycutils.nlp.bigvocab.VocabularyMap(corpus_vocabulary=ycutils.nlp.corpus.CorpusVocabulary(C, unknown_token=None), create_token=False)
    
    BV.to_file(sys.stdout)

    print 'BV.get_indexes([\'new\', \'world\']) = {}'.format(BV.get_indexes(['new', 'world']))
    print 'BV.get_tokens([5, 1]) = {}'.format(BV.get_tokens([5, 1]))
    # BV.get_indexes(['new', 'world']) = [5, 1]
    # BV.get_tokens([5, 1]) = ['__UNK__', 'world']
    
    Bx = ycutils.nlp.bagofwords.Document('Bx', ycutils.nlp.bigvocab.BigBOW(BV, bow=ycutils.nlp.bagofwords.BOW('this is a very new world'.split())))
    print 'Bx = {}'.format(Bx)
    print 'Bx = {}'.format(BV.keys_to_tokens(Bx))
    # all unknown words will be put under __UNK__
    # Bx = Bx 0:1 1:1 2:1 5:3
    # Bx = {'this': 1, 'world': 1, 'is': 1, '__UNK__': 3}

*VocabularyMap* class
=====================
.. autoclass:: ycutils.nlp.bigvocab.VocabularyMap
  :members:

*BigBOW* class
==================
.. autoclass:: ycutils.nlp.bigvocab.BigBOW
  :members:
"""

import ycutils.nlp.bagofwords, ycutils.io.tsvio

class VocabularyMap:
  """This class handles mappings between integer keys and the tokens they represent.

  :param corpus_vocabulary: populate the object with tokens from a :class:`ycutils.nlp.corpus.CorpusVocabulary`.
  :param create_token: sets the default behaviour when retrieving index for tokens we have not seen before, to create a new index or to return the unknown token (if one exist)
  :param unknown_token: use this token as the unknown token. Set to `None` to ignore unknown tokens (i.e raise an error on unknown terms). Otherwise, an unknown token will be created.

  .. seealso:: :meth:`get_index` for detailed behaviour when we encounter a token that is not found in the vocabulary."""

  __index = None
  __tokens = None

  create_token = False
  """Defines the default behaviour when we encounter a never seen before token."""

  unknown_token = '__UNK__'
  """The type for an unknown token, which is ``__UNK__`` by default."""

  def __init__(self, from_filename=None, corpus_vocabulary=None, create_token=False, unknown_token='__UNK__'):
    self.__index = {}
    self.__tokens = []

    if corpus_vocabulary:
      for w in corpus_vocabulary:
        if w in self.__index: continue

        self.__index[w] = len(self.__tokens)
        self.__tokens.append(w)
      #end for

    elif from_filename:
      with open(from_filename, 'r') as f:
        self.from_file(f, unknown_token=unknown_token)
    #end if

    if unknown_token and unknown_token not in self.__index:
      self.__index[unknown_token] = len(self.__tokens)
      self.__tokens.append(unknown_token)
    #end if

    self.create_token = create_token
    self.unknown_token = unknown_token
  #end def

  def add_bow(self, bow):
    """Adds words from a :class:`ycutils.nlp.bagofwords.BOW` (or any :class:`collections.Counter` for that matter).

    :param bow: bag of words to add to vocabulary, creating new mappings for new words."""
    self.add_tokens(bow.iterkeys())
  #end def

  def add_tokens(self, tokens):
    """Adds tokens from a iterable of string.

    :param tokens: iterable of tokens to add to vocabulary, creating new mappings for new tokens."""

    for w in tokens:
      if w in self.__index: continue

      self.__index[w] = len(self.__tokens)
      self.__tokens.append(w)
    #end for
  #end def

  def get_index(self, token):
    """Finds the index of the token given.

    :param token: the token to find the index for.
    :returns: an index to the given token.

    .. note:: On encountering a never seen before token, if :attr:`create_token` is set to `True`, a new index will be created and returned. Else if :attr:`unknown_token` is defined (not `None`), the index for :attr:`unknown_token` will be returned. If :attr:`unknown_token` is set to `None`, a :exc:`KeyError` will be raised."""

    if token in self.__index: return self.__index[token]

    if self.create_token: # add the token to the vocabulary
      self.add_tokens([token])
      return self.__index[token]
    #end if

    if self.unknown_token: return self.__index[self.unknown_token]

    raise KeyError(u'\'{}\' not found in vocabulary.'.format(token))
  #end def

  def get_indexes(self, tokens):
    """Finds the index of tokens in a given list.

    :param tokens: a list of tokens to find the index for.
    :returns: a list of indexes corresponding to the list of tokens.

    .. seealso:: :meth:`get_index` for the behaviour when we encounter a token that is not found in the vocabulary."""

    return map(lambda w: self.get_index(w), tokens)
  #end def

  def get_token(self, index):
    """Finds the token at the index position.

    :param index: the index to retrieve the token.
    :returns: token with the given index."""

    return self.__tokens[index]
  #end def

  def get_tokens(self, indexes):
    """Finds the tokens at the given indexes.

    :param indexes: the indexes to retrieve the tokens.
    :returns: tokens with the given indexes."""

    return map(lambda i: self.__tokens[i], indexes)
  #end def

  def keys_to_tokens(self, d):
    """Takes a dictionary and transforms all its key from indexes to tokens.

    :param d: dictionary to do the transformation on."""

    return dict([(self.get_token(k), v) for k, v in d.iteritems()])
  #end def

  def keys_to_indexes(self, d, sum_unknown=True):
    """Takes a dictionary and transforms all its key from tokens to indexes.

    .. warning:: Assuming :attr:`unknown_token` is defined. If :attr:`sum_unknown` is set to False, all unknown tokens are placed together under the index for :attr:`unknown_token`, and its corresponding value will not be defined.

    .. seealso:: :meth:`get_index` for the behaviour when we encounter a token that is not found in the vovabulary.

    :param d: dictionary to do the transformation on.
    :param sum_unknown: performs a sum over values that are mapped to unknown."""

    kv = [(self.get_index(k), v) for k, v in d.iteritems()]

    new_d = dict(kv)
    if self.unknown_token and sum_unknown:
      unknown_i = self.__index[self.unknown_token]
      new_d[unknown_i] = sum([v for k, v in kv if k == unknown_i])
    #end if

    return new_d
  #end def

  def to_file(self, f):
    """Writes out the vocabulary mapping (indexes and tokens) to file descriptor.

    File format is tab separated values with index and token columns, one token per line.

    :param f: file descriptor to write to."""

    t = ycutils.io.tsvio.TSVFile()

    t.writeline(f, 'vocab_count={}'.format(self.size()), comment=True)
    t.writeline(f, 'index\ttoken', comment=True)
    for i, w in enumerate(self.__tokens): t.writeline(f, [i, w])
  #end def

  def from_file(self, f, unknown_token='__UNK__'):
    """Reads from file descriptor a previous saved file using :meth:`to_file`.

    :param f: file descriptor to read from.
    :param unknown_token: unknown token that is used in this file."""

    self.__index = {}
    self.__tokens = []

    t = ycutils.io.tsvio.TSVFile()
    for index, token in t.readlines(f):
      index = int(index)
      if len(self.__tokens) == index:
        self.__tokens.append(token)
        self.__index[token] = index
      else:
        raise ValueError('Vocabulary index must be consecutive. Missing index = {}'.format(index))
    #end for
  #end def

  def size(self): 
    """:returns: number of tokens in the vocabulary."""
    return len(self.__tokens)
    
  def __len__(self): 
    """:returns: number of tokens in the vocabulary."""
    return len(self.__tokens)
#end class

class BigBOW(ycutils.nlp.bagofwords.BOW):
  """A derived :class:`ycutils.nlp.bagofwords.BOW` class which is able to transparently handle tokens and indexes mapping.

  :param vocabulary: the vocabulary mapping file that we use.
  :param tokens: iterable list of tokensto add to the bag of word.
  :param wc_string: initialize bag of words with a word:count formatted string.

  .. note:: If vocabulary has :attr:`Vocabulary.create_token` set to ``True``, the vocabulary can be created on the fly by adding word to this :class:`BigBOW`.

  .. note:: Standard dictionary access through :meth:`__getitem__()`, :meth:`__iter__()`, etc will use the indexes as keys. To access using tokens, use the wrapper functions :meth:`itertokens`, :meth:`tokens`, :meth:`set_token_count`, :meth:`get_token_count`, :meth:`inc_token_count` and :meth:`delete_token`."""

  __vocabulary = None

  def __init__(self, vocabulary, tokens=None, wc_string=None, bow=None):
    self.__vocabulary = vocabulary

    if tokens: self.add_tokens(tokens)
    if wc_string: self.add_wc_string(wc_string)
    if bow: self.add_bow(bow)
  #end def

  def add_bow(self, bow):
    """Adds a bag of word counts to our object, transparently converting the keys."""

    self.update(self.__vocabulary.keys_to_indexes(bow))
  #end def

  def add_tokens(self, tokens):
    """Converts given list of tokens to indexes and adds them to the object.

    :param tokens: the list of tokens to add to the :class:`BigBOW`."""

    self.update(self.__vocabulary.get_indexes(tokens))
  #end def

  def add_wc_string(self, s):
    """Adds tokens and their counts from a ``word:count`` formatted string, transparently converting them to indexes.

    :param s: a string of the form in ``word:count`` format."""

    for wc_str in s.split():
      w, c = wc_str.split(':')
      self[self.__vocabulary.get_index(w)] += float(c)
    #end for
  #end def

  def to_wc_string(self):
    """Format the :class:`BigBOW` object in a ``word:count`` formatted string which looks like ``word1:count1 word2:count2 ...``.

    :returns: the formatted string."""

    return ' '.join([u'{}:{}'.format(self.__vocabulary.get_token(w), c) for w, c in self.iteritems()])
  #end def

  def itertokens(self):
    """:returns: an iterator object for the tokens in this object (not indexes, which is the default :meth:`__iter__()` behaviour)."""
    return BigBOW.TokenIter(self.__vocabulary, self.__iter__())
  #end def

  def tokens(self):
    """:returns: a list of tokens in this object, instead of indexes, as in :meth:`keys()`."""
    return map(lambda i: self.__vocabulary.get_word(i), self.iterkeys())
  #end def

  def set_token_count(self, token, freq):
    """Sets the frequency of ``token`` in this object."""

    self[self.__vocabulary.get_index(token)] = freq
  #end def

  def inc_token_count(self, token, inc):
    """Increment the frequency of ``token`` in this object."""

    self[self.__vocabulary.get_index(token)] += inc
  #end def

  def get_token_count(self, token):
    """:returns: the frequency of ``token`` in this object."""
    return self[self.__vocabulary.get_index(token)]
  #end def

  def delete_token(self, token):
    """Deletes ``token`` from this object."""
    del self[self.__vocabulary.get_index(token)]
  #end def

  class TokenIter:
    """The :class:`Iterator` object returned by :meth:`itertokens`, which maps indexes to tokens."""

    __vocabulary = None
    __iter_obj = None

    def __init__(self, vocabulary, iter_obj):
      self.__vocabulary = vocabulary
      self.__iter_obj = iter_obj
    #end def

    def __iter__(self): return self

    def next(self):
      i = self.__iter_obj.next()
      return self.__vocabulary.get_token(i)
    #end def
  #end class
#end class
