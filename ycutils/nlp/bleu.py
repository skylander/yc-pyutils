"""
*************
*bleu* module
*************

This module contains methods for computing the BLEU precision/recall from two given list of tokens. It also contains a useful method for counting ngram occurrences.

See `BLEU: a Method for Automatic Evaluation of Machine Translation (Papineni et al, 2002) <http://dl.acm.org/citation.cfm?id=1073135>`_ for more information about the BLEU metric.

Methods
=======
.. autofunction:: score
.. autofunction:: count_ngrams
"""

import collections
import numpy as np

def count_ngrams(tokens, n, all_smaller=False):
  """Counts the frequency of n-grams in the given list of tokens.

  :param tokens: list of tokens to compute ngrams for.
  :param n: number of grams to count.
  :param all_smaller: set to True to include all n-grams from n=1 to n.
  """

  counts = collections.Counter()
  for k in xrange(1 if all_smaller else n, n+1):
    for i in xrange(len(tokens)-k+1):
      counts[tuple(tokens[i:i+k])] += 1

  return counts
#end def

def score(ref_ngrams, ref_len, pred_ngrams, pred_len, n):
  """Calculate the BLEU precision and recall from ngram counts.

  :param ref_ngrams: reference sentence ngrams.
  :param ref_len: reference sentence length.
  :param pred_ngrams: predicted sentence ngrams.
  :param pred_len: predicted sentence length.
  :param n: the maximum number of ngrams to consider.
  """

  if not ref_len or not pred_len: return 0.0, 0.0
  if not len(ref_ngrams) or not len(pred_ngrams): return 0.0, 0.0

  ngram_score = np.zeros(n, dtype=np.float32) + 0.1

  # compute the ngram intersections
  for ngram, c in ref_ngrams.iteritems():
    if len(ngram) > n: continue

    k = min(c, pred_ngrams[ngram])
    ngram_score[len(ngram) - 1] += k
  #end for

  # compute the geometric mean of the ngrams precision/recall
  precision = np.mean(np.log(ngram_score / len(pred_ngrams)))
  recall = np.mean(np.log(ngram_score / len(ref_ngrams)))

  # apply the brevity penalty
  if pred_len <= ref_len: precision += 1.0 - (float(ref_len) / pred_len)
  if ref_len <= pred_len: recall += 1.0 - (float(pred_len) / ref_len)

  precision = np.exp(precision)
  recall = np.exp(recall)

  return precision, recall
#end def
