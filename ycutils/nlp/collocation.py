import itertools, math
import scipy.sparse
import numpy as np

class Statistics:
  """Data structure for building collocation statistics on the go. Only accept indexes and not words."""

  __allowed_indexes = None
  _vocab = None
  _collocation_matrix = None
  _doc_freq = None
  _D = None

  LAPLACE_SMOOTHING_COLLOCATION = 1e-12
  LAPLACE_SMOOTHING_DOC_FREQ = .001
  PMI_THRESHOLD = .00001

  def __init__(self, vocabulary_mapping, allowed_indexes=None):
    self._vocab = vocabulary_mapping
    self.__allowed_indexes = allowed_indexes
    self._collocation_matrix = scipy.sparse.dok_matrix((len(vocabulary_mapping), len(vocabulary_mapping)), dtype=np.int32)
    self._doc_freq = np.zeros(len(vocabulary_mapping), dtype=np.int32)
    self._D = 0
  #end def

  def add_corpus(self, corpus):
    for doc in corpus.itervalues(): self.add_big_bow(doc)
  #end def

  def add_big_bow(self, bow):
    for x, y in itertools.combinations(bow.keys(), 2):
      if self.__allowed_indexes and (x not in self.__allowed_indexes or y not in self.__allowed_indexes): continue # skip since x or y not allowed

      self.update_collocation_stats(x, y, 1)
    #end for

    for x in bow.iterkeys():
      try:
        self._doc_freq[x] += 1
      except IndexError:
        self._doc_freq.resize(len(self._vocab))
        self._doc_freq[x] += 1
      #end try

    self._D += 1
  #end def

  def update_collocation_stats(self, x, y, c):
    try:
      if x > y: self._collocation_matrix[y, x] += c
      else: self._collocation_matrix[x, y] += c
    except IndexError:
      self._collocation_matrix.resize((len(self._vocab), len(self._vocab)))

      if x > y: self._collocation_matrix[y, x] += c
      else: self._collocation_matrix[x, y] += c
    #end try
  #end def

  def utilization(self):
    return float(len(self._collocation_matrix)) / (0.5 * len(self._vocab) * (len(self._vocab) - 1)) * 100.0

  def __str__(self):
    return '<Statistics: {} docs, {} types observed, {} entries in collocation matrix, {:.2f}% utilization>'.format(self._D, np.count_nonzero(self._doc_freq), len(self._collocation_matrix), self.utilization())

  def to_file(self, f):
    pass
  #end def

  def get_collocation_stat(self, x, y):
    if x < y: return self._collocation_matrix[x, y]
    return self._collocation_matrix[y, x]
  #end def

#end class
class PMIMatrix(Statistics):
  __pmi = None
  __no_collocate_k1 = None
  __no_collocate_k2 = None
  
  def build_pmi(self):
    log_D_plus_k_lambda = math.log(self._D + self.LAPLACE_SMOOTHING_COLLOCATION * (0.5 * len(self._vocab) * (len(self._vocab) - 1.0)))
    log_D_plus_lambda = math.log(self._D + self.LAPLACE_SMOOTHING_DOC_FREQ)
    log_constant = log_D_plus_lambda + log_D_plus_lambda - log_D_plus_k_lambda
    
    # print self._collocation_matrix
    # print self._doc_freq

    pmi = scipy.sparse.dok_matrix((len(self._vocab), len(self._vocab)), dtype=np.float32)
    for (i, j), c in self._collocation_matrix.iteritems():
      p = log_constant + math.log(c + self.LAPLACE_SMOOTHING_COLLOCATION) - math.log(self._doc_freq[i] + self.LAPLACE_SMOOTHING_DOC_FREQ) - math.log(self._doc_freq[j] + self.LAPLACE_SMOOTHING_DOC_FREQ)
      p /= log_D_plus_k_lambda - math.log(c + self.LAPLACE_SMOOTHING_COLLOCATION) # normalize
      
      if abs(p) > self.PMI_THRESHOLD: pmi[i, j] = p
    #end for
      
    # c = 0.0
    # i = 0
    # j = 3
    # pmi[i, j] = log_constant + math.log(c + self.LAPLACE_SMOOTHING_COLLOCATION) - math.log(self._doc_freq[i] + self.LAPLACE_SMOOTHING_DOC_FREQ) - math.log(self._doc_freq[j] + self.LAPLACE_SMOOTHING_DOC_FREQ)
    # pmi[i, j] /= log_D_plus_k_lambda - math.log(c + self.LAPLACE_SMOOTHING_COLLOCATION)
    
    self.__no_collocate_k1 = log_constant + math.log(self.LAPLACE_SMOOTHING_COLLOCATION)
    self.__no_collocate_k2 = log_D_plus_k_lambda - math.log(self.LAPLACE_SMOOTHING_COLLOCATION)

    # print pmi
    self.__pmi = pmi
  #end def
  
  def get_pmi(self, x, y):
    if not self.__pmi: raise ValueError('call build_pmi first!')
    
    ij = (x, y) if x < y else (y, x)
    if ij in self.__pmi: return self.__pmi[ij]
    p = self.__no_collocate_k1 - math.log(self._doc_freq[x] + self.LAPLACE_SMOOTHING_DOC_FREQ) - math.log(self._doc_freq[y] + self.LAPLACE_SMOOTHING_DOC_FREQ)
    p /= self.__no_collocate_k2
    
    return p
  #end def
#end class
