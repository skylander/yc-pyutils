"""
*******************
*corpus* module
*******************

This module contains classes and methods that are useful when handling corpora.

:class:`Corpus` handles collections of :class:`Document`s or :class:`BOW`s, while :class:`CorpusVocabulary` handles collections of vocabulary terms (including extra information like term, document and inverse document frequencies.

Example:

..  code-block:: python

    import ycutils.nlp.bagofwords
    import ycutils.nlp.corpus

    # Corpus class
    x = ycutils.nlp.bagofwords.Document('x', ['hello', 'hello', 'world'])
    y = ycutils.nlp.bagofwords.Document('y', ['bye', 'bye', 'world'])
    z = ycutils.nlp.bagofwords.Document('z', ['this', 'bye', 'hello'])

    C = ycutils.nlp.corpus.Corpus()
    C.add_document(x)
    C.add_bow(y)
    C.add_document(z)

    C.to_file(sys.stdout)
    print C.vocabulary()
    print C.document_frequency()

    # doc_count=3
    # title  wc_string
    # 0x13fab206d2da1b9f      world:1 bye:2
    # x       world:1 hello:2
    # z       this:1 bye:1 hello:1
    # Counter({'bye': 3, 'hello': 3, 'world': 2, 'this': 1})
    # Counter({'world': 2, 'bye': 2, 'hello': 2, 'this': 1})

    # CorpusVocabulary class
    CV = ycutils.nlp.corpus.CorpusVocabulary(C, unknown_token='__UNK__')
    CV.to_file(sys.stdout, ['freq-'], save_unknown=True)
    print CV.find_token('new_word')

    # vocab_size=5
    # sort_key=['freq-']
    # unknown_token=__UNK__
    # index  token  freq    df
    # 0      __UNK__ 0       0
    # 1      this    1       1
    # 2      world   2       2
    # 3      bye     3       2
    # 4      hello   3       2
    # (0, 0)

Attributes
==========
.. autodata:: DEFAULT_UNKNOWN_TOKEN

*Corpus* class
================
.. autoclass:: ycutils.nlp.corpus.Corpus
  :members:

*CorpusVocabulary* class
========================
.. autoclass:: ycutils.nlp.corpus.CorpusVocabulary
  :members:
  :special-members: __init__, __getitem__, __iter__, __contains__, __len__
"""
import collections
import math
import codecs
import ycutils.nlp.bagofwords
import ycutils.io.tsvio

DEFAULT_UNKNOWN_TOKEN = u'__UNK__'
"""The type for an unknown token, which is ``__UNK__`` by default."""


class Corpus(dict):
  """A dictionary collection of :class:`ycutils.nlp.bagofwords.Document` objects."""

  #: .. note:: A large setting will lead to a more uniform weightage over words.
  #: .. seealso:: :meth:`inverse_document_frequency`
  IDF_LAPLACE_SMOOTHING = .001

  def __str__(self):
    return '<Corpus with {} documents>'.format(len(self))

  def add_document(self, doc):
    """Adds a document to the corpus.

    :param doc: Document to be added to the corpus.

    .. note:: This method does not check for existing titles. Duplicate titles will be replaced."""

    self[doc.title] = doc.bow()
  #end def

  def add_bow(self, bow, title=None):
    """Adds a :class:`BOW` to the corpus.

    :param title: title of document (defaults to a random hexadecimal string).
    :param bow: bag of words object."""

    if not title: title = self.unique_title()

    self[title] = bow.bow() if isinstance(bow, ycutils.nlp.bagofwords.Document) else bow
  #end def

  def unique_title(self):
    """:returns: a title that is unique to this corpus."""
    title = ycutils.nlp.bagofwords.random_title()
    while title in self:
      title = ycutils.nlp.bagofwords.random_title()

    return title
  #end def

  def to_file(self, f, sort=True):
    """Writes out corpus to a file descriptor in textual format.

    :param f: file description to write to.
    :param sort: whether to save document according to their titles"""

    tsv_corpus = ycutils.io.tsvio.TSVFile()
    tsv_corpus.writeline(f, 'IDF_LAPLACE_SMOOTHING={}'.format(self.IDF_LAPLACE_SMOOTHING), comment=True)
    tsv_corpus.writeline(f, 'doc_count={}'.format(len(self)), comment=True)
    tsv_corpus.writeline(f, 'title\twc_string', comment=True)

    title_list = sorted(self.iterkeys()) if sort else self.iterkeys()
    for title in title_list: tsv_corpus.writeline(f, (title, self[title].to_wc_string()))
  #end def

  def from_file(self, f):
    """Reads a :class:`Corpus` object from a text file.

    :param f: file to read from."""

    for line in f:
      line = line.strip()
      if not line: continue
      self.add_document(ycutils.nlp.bagofwords.Document(wc_string=line))
    #end for
  #end def

  def vocabulary(self):
    """Builds a :class:`Counter` of vocabulary (and frequency) used.

    :returns: vocabulary and their corpus frequency.
    :rtype: :class:`Counter`"""

    V = collections.Counter()

    for bow in self.itervalues():
      for w, c in bow.iteritems():
        V[w] += c

    return V
  #end def

  def inverse_document_frequency(self, df=None):
    """Builds a :class:`Counter` of vocabulary and their inverse document frequency.

    :param df: document frequency in the form of a :class:`Counter`, returned by :meth:`document_frequency`.
    :returns: vocabulary and their idf values.
    :rtype: :class:`Counter`

    .. note:: A small constant, :attr:`IDF_LAPLACE_SMOOTHING`, is added to each term's document frequency to avoid divide by zero errors.
    """

    idf = df if df else self.document_frequency()
    log_D = math.log(len(self) + (len(idf) * self.IDF_LAPLACE_SMOOTHING))
    for w, c in idf.iteritems(): idf[w] = log_D - math.log(c + self.IDF_LAPLACE_SMOOTHING)

    return idf
  #end def

  def document_frequency(self):
    """Builds a :class:`Counter` of vocabulary and their document frequencies.

    :returns: vocabulary and their df values.
    :rtype: :class:`Counter`"""

    df = collections.Counter()
    for bow in self.itervalues():
      for w in bow.iterkeys():
        df[w] += 1

    return df
  #end def
#end class


class CorpusVocabulary:
  """A class that handles vocabulary information related to a corpus, like term frequencies, etc.

  When loading vocbabulary from file, two formats are supported.
  #. A list of terms, one on each line, or
  #. our in-house vocabulary file format.

  :param corpus: :class:`Corpus` object to build :class:`CorpusVocabulary` from.
  :param from_filename: path to text file where we can load vocabulary information from.
  :param from_file: file-ob where we can load vocabulary information from.
  :param unknown_token: the type to use for an unknown token. If set to `None`, an error will be raised whenever an unknown token is encountered."""

  __vocab = {}
  __document_count = 0

  unknown_token = DEFAULT_UNKNOWN_TOKEN

  def __init__(self, corpus=None, from_filename=None, from_file=None, unknown_token=DEFAULT_UNKNOWN_TOKEN):
    self.unknown_token = unknown_token

    if corpus: self.from_corpus(corpus)
    elif from_filename:
      with codecs.open(from_filename, 'r', 'utf-8') as f: self.from_file(f)
    elif from_file:
      self.from_file(from_file)
  #end def

  def __iadd__(self, other):
    """Adds two :class:`CorpusVocabulary` in place.

    :param other: the other :class:`CorpusVocabulary` object to add."""

    all_tokens = set(self.__vocab.keys())
    all_tokens.update(set(other.__vocab.keys()))

    self.__document_count += other.__document_count

    log_D = math.log(self.__document_count + (len(all_tokens) * Corpus.IDF_LAPLACE_SMOOTHING) + Corpus.IDF_LAPLACE_SMOOTHING)  # one more for the unknown token
    for w in all_tokens:
      tf, df = 0, 0
      if w in self.__vocab: tf, df, idf = self.__vocab[w]
      if w in other.__vocab:
        xtf, xdf, idf = other.__vocab[w]
        tf += xtf
        df += xdf
      #end if

      self.__vocab[w] = (tf, df, log_D - math.log(df + Corpus.IDF_LAPLACE_SMOOTHING))
    #end for

    if self.unknown_token: self.__vocab.setdefault(self.unknown_token, (0, 0, log_D - math.log(Corpus.IDF_LAPLACE_SMOOTHING)))  # unknown token appears minimal times

    return self
  #end def

  def __getitem__(self, token):
    """Returns statistics about the given token in the corpus.

    :param token: token to get statistics for.
    :returns: a tuple `(frequency, document frequency, inverse document frequency)`.

    .. note:: An error will be raised if the token is not found in the vocabulary."""

    return self.__vocab[token]
  #end def

  def __delitem__(self, token):
    """Remove token from vocabulary.

    :param token: token to remove.

    .. note:: An error will be raised if the token is not found in the vocabulary."""

    del self.__vocab[token]
  #end def

  def __iter__(self):
    """Returns an iterator for going through each word type in the vocabulary."""
    return self.__vocab.iterkeys()
  #end def

  def __contains__(self, w):
    """:returns: ``True`` if :attr:`w` is in the vocabulary."""
    return w in self.__vocab
  #end def

  def __len__(self):
    """Returns the number of terms in the vocabulary."""
    return len(self.__vocab)
  #end def

  def iteritems(self):
    """Returns an iterator for a tuple of `(word, (frequency, document frequency, inverse document frequency))`."""
    return self.__vocab.iteritems()
  #end def

  def document_count(self):
    """Returns the number of documents that make up this vocabulary."""
    return self.__document_count
  #end def

  def find_token(self, token):
    """Retrieves statistics for the given token, and default statistics if the token is not found.

    :param token: token to get statistics for.
    :returns: a tuple `(frequency, document frequency, inverse document frequency)`. If token is not found, statistics for :attr:`unknown_token` is returned."""

    if token in self.__vocab: return self.__vocab[token]

    if not self.unknown_token: raise KeyError('\'{}\' not found in corpus vocabulary.'.format(token))

    return self.__vocab[self.unknown_token]
  #end def

  def from_corpus(self, corpus):
    """Build our vocabulary information from a given :class:`Corpus` object.

    :param corpus: :class:`Corpus` object."""

    V_tf = corpus.vocabulary()
    V_df = corpus.document_frequency()

    self.__vocab = {}
    self.__document_count = len(corpus)

    log_D = math.log(len(corpus) + (len(V_df) * corpus.IDF_LAPLACE_SMOOTHING) + corpus.IDF_LAPLACE_SMOOTHING)  # one more for the unknown token
    for w, c in V_tf.iteritems():
      df = V_df[w]
      self.__vocab[w] = (c, df, log_D - math.log(df + corpus.IDF_LAPLACE_SMOOTHING))
    #end for

    if self.unknown_token: self.__vocab.setdefault(self.unknown_token, (0, 0, log_D - math.log(corpus.IDF_LAPLACE_SMOOTHING)))  # unknown token appears minimal times
  #end def

  def to_file(self, f, sort_key=['idf+', 'freq+'], save_unknown=False):
    """Writes out corpus vocabulary to a file descriptor in textual format.

    File format is a tab separated values file (see :class:`tsvio.TSVFile`), and the columns are: tokens, frequency in corpus, no. of documents containing token and inverse document frequency.

    :param f: file description to write to.
    :param sort_key: sort order to use for vocabulary. Possible options are `idf` and `freq`. A `+` at the back denotes largest first (descending order) and `-` for ascending order. Defaults to descending order.
    :param save_unknown: saves the default statistics for :attr:`unknown_token` to file."""

    for i, key in enumerate(sort_key):
      if key == 'idf': sort_key[i] = 'idf+'
      elif key == 'freq': sort_key[i] = 'freq+'
      elif not key.startswith('idf') and not key.startswith('freq'): raise ValueError('Unknown sort key for corpus vocabulary\'s terms')
    #end for

    sort_list = []
    for (w, (c, df, idf)) in self.__vocab.iteritems():
      if not save_unknown and w == self.unknown_token: continue

      row = []
      for key in sort_key:
        if key == 'idf+': row.append(-idf)
        elif key == 'idf-': row.append(idf)
        elif key == 'freq+': row.append(-c)
        elif key == 'freq-': row.append(c)
      #end for

      row.append(w)

      sort_list.append(tuple(row))
    #end for

    tsv_vocab = ycutils.io.tsvio.TSVFile()

    tsv_vocab.writeline(f, 'vocab_size={}'.format(len(sort_list)), comment=True)
    tsv_vocab.writeline(f, 'document_count={}'.format(self.__document_count), comment=True)
    tsv_vocab.writeline(f, 'sort_key={}'.format(sort_key), comment=True)
    tsv_vocab.writeline(f, 'unknown_token={}'.format(self.unknown_token), comment=True)
    tsv_vocab.writeline(f, 'token\tfreq\tdf\tidf', comment=True)
    # tsv_vocab.writeline(f, 'IDF_LAPLACE_SMOOTHING={}'.format(corpus.IDF_LAPLACE_SMOOTHING), comment=True)

    for row in sorted(sort_list):
      w = row[-1]
      (c, df, idf) = self.__vocab[w]
      tsv_vocab.writeline(f, (w, c, df, idf))
    #end for
  #end def

  def from_file(self, f):
    """Reads a :class:`CorpusVocabulary` object from a file.

    See :meth:`to_file` for information on file format.

    :param f: file to read from."""

    self.__vocab = {}
    tsv_vocab = ycutils.io.tsvio.TSVFile()
    for token_info in tsv_vocab.readlines(f):
      try: (w, c, df, idf) = token_info
      except ValueError: (w, c, df, idf) = token_info[0], 0.0, 0.0, 0.0
      #end if
      self.__vocab[w] = (float(c), float(df), float(idf))

    if self.unknown_token: self.__vocab.setdefault(self.unknown_token, (0.0, 0.0, 0.0))
  #end def

  def filter(self, minimums=(None, None, None), maximums=(None, None, None), remove_terms=[]):
    """Remove words that are not in the range between ``minimums`` and ``maximums``.

    :param minimums: a 3-tuple containing the minimum (word frequency, document frequency, IDF). Use ``None`` if no minimums are defined.
    :param maximums: a 3-tuple containing the maximum (word frequency, document frequency, IDF). Use ``None`` if no minimums are defined.
    :param remove_terms: an iterable of terms to remove from the vocabulary."""

    min_c, min_df, min_idf = minimums
    max_c, max_df, max_idf = maximums

    delete = set()
    for w, (c, df, idf) in self.__vocab.iteritems():
      if min_c and c < min_c: delete.add(w)
      elif min_df and df < min_df: delete.add(w)
      elif min_idf and idf < min_idf: delete.add(w)

      elif max_c and c > max_c: delete.add(w)
      elif max_df and df > max_df: delete.add(w)
      elif max_idf and idf > max_idf: delete.add(w)
    #end for

    for w in delete: del self.__vocab[w]
    for w in remove_terms:
      if w in self.__vocab:
        del self.__vocab[w]
  #end def

  def to_bow(self, use_counts='freq'):
    """Creates a :class:`bagofwords.BOW` object from this vocabulary.

    .. note:: __UNK__ tokens will be ignored.

    :params use_counts: type of frequency to use for bag of words counts, can be ``freq`` (default), ``df`` or ``idf``.

    :returns: a :class:`bagofwords.BOW` object containing words and counts from this vocabulary.
    """

    bow = ycutils.nlp.bagofwords.BOW()
    for w, (c, df, idf) in self.__vocab.iteritems():
      if w == self.unknown_token or c == 0: continue
      if use_counts == 'idf': bow[w] = idf
      elif use_counts == 'df': bow[w] = df
      else: bow[w] = c
    #end for

    return bow
  #end def
#end class
