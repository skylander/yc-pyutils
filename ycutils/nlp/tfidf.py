"""
**************
*tfidf* module
**************

This module contains classes and methods for computing TF-IDF values.

Example:

..  code-block:: python

    import ycutils.nlp.bagofwords
    import ycutils.nlp.tfidf

    t = ycutils.nlp.tfidf.TFIDF(corpus=C)

    x = ycutils.nlp.bagofwords.Document('x', ['hello', 'hello', 'world'])

    print x
    print t.transform(x)
    print t.untransform(x)

    # x       world:1 hello:2
    # x       world:0.406630512386 hello:0.813261024771
    # x       world:1.0 hello:2.0

*TFIDF* class
=============
.. autoclass:: ycutils.nlp.tfidf.TFIDF
  :members: transform, untransform
"""

import ycutils.nlp.corpus

class TFIDF:
  """This class is a wrapper class for computing TF-IDF values of :class:`ycutils.nlp.bagofwords.BOW` or :class:`ycutils.nlp.bagofwords.Document`.

  :param corpus: corpus to initialize with, we will calculate the tf-idf statistics we need by passing it through the :class:`ycutils.nlp.corpus.CorpusVocabulary` class.
  :param corpus_vocabulary: use this :class:`ycutils.nlp.corpus.CorpusVocabulary` for IDF values.

  .. warning: Changes to the underlying corpus_vocabulary will affect future tfidf calculations.
  """

  corpus_vocabulary = None
  """The corpus vocabulary used by the object to calculate TF-IDF values."""

  def __init__(self, corpus=None, corpus_vocabulary=None):
    if corpus: self.corpus_vocabulary = ycutils.nlp.corpus.CorpusVocabulary(corpus)

    if corpus_vocabulary: self.corpus_vocabulary = corpus_vocabulary
  #end def

  def transform(self, bow):
    """Performs TF-IDF transform on the given bag of words vector.

    :param bow: bag of words vector (a :class:`Counter` class).

    .. note:: The vector is modified in place."""

    for w in bow.iterkeys():
      (freq, df, idf) = self.corpus_vocabulary.find_token(w)
      bow[w] *= idf
    #end for

    return bow
  #end def

  def untransform(self, bow):
    """Reverses the TF-IDF transform on the given bag of words vector.

    :param bow: bag of words vector (a :class:`Counter` class).

    .. note:: The vector is modified in place."""

    for w in bow.iterkeys():
      (freq, df, idf) = self.corpus_vocabulary.find_token(w)
      bow[w] /= idf
    #end for

    return bow
  #end def
#end class
