"""
*****************
*tokenize* module
*****************

This module contains function to split a piece of text into individual sentences (using `Splitta <http://code.google.com/p/splitta/>`_) and into individual words (using many regexes).

Sample text:
  `At 2PM today, In fairness, the national networks did show 74,730st other images, especially shots of Lake Pontchartrain spilling out onto Lakeshore Drive or traffic signs twisting in the wind, but there was no information about when the images were recorded. Is this what the wind is like now? Three hours ago? Or as far back as yesterday? I can watch the same sports highlights on ESPN over and over again and never be bored by the repetition. The difference here, though, is that I know that Lebron's blocked shot, Drew's pinpoint touchdown pass, Tiger's escape from the bunker isn't still happening. But watching the Weather Channel hour after hour - and what else does one concerned about the city do but that? - one has a hard time separating the past from the present, distinguishing the weather that was from the weather that is. You'd also have a hard time separating flooding that's perilous from the flooding that's inconvenient. What do these reporters mean when they say it's flooding? Is it water up to the headlights or water into raised houses? Water that people cross with rubber boots? Or water requiring a pirogue? Is it an amount we see every few years or an amount we see every Wednesday? So long as it wasn't an amount caused by busted levees I knew the city would survive. Jarvis DeBerry can be reached at jdeberry@timespicayune.com or (504) 826.3355. Follow him at http://connect.nola.com/user/jdeberry/index.html and at twitter.com/jerry.`

Example:

.. code-block:: python

   import ycutils.nlp.tokenize

   sents = ycutils.nlp.tokenize.sentences(text)
   tokens = ycutils.nlp.tokenize.words_in_sentences(sents)

Methods
=======
.. autofunction:: sentences
.. autofunction:: words
.. autofunction:: words_in_sentences
.. autofunction:: ngram_tokens
.. autofunction:: stem_tokens
.. autofunction:: filter_stopwords
.. autofunction:: tag_tokens
.. autofunction:: to_ascii

Attributes
==========
.. _ignore_tags:

List of tags
------------
.. autodata:: TAG_EMPTY
.. autodata:: TAG_SEPARATOR
.. autodata:: TAG_PUNCT
.. autodata:: TAG_SYMBOL
.. autodata:: TAG_WORD
.. autodata:: TAG_NUM
.. autodata:: TAG_DATE
.. autodata:: TAG_TIME
.. autodata:: TAG_PHONE
.. autodata:: TAG_EMAIL
.. autodata:: TAG_URL
.. autodata:: TAG_MONEY

Default tokenization options
----------------------------
.. autodata:: __DEFAULT_NORMALIZE__
.. autodata:: __DEFAULT_IGNORE_TAGS__
"""

import operator
import os
import regex as re  # required

from PorterStemmer import PorterStemmer

MONTHS = set(map(unicode, ['feb', 'oct', 'mar', 'aug', 'sep', 'jan', 'jun', 'apr', 'dec', 'jul', 'nov', 'january', 'february', 'march', 'april', 'may', 'june', 'july', 'august', 'september', 'october', 'november', 'december']))
"""Month names that will be tagged as :attr:`TAG_DATE`."""

STOPWORDS = set(map(unicode, ['a', 'a\'s', 'able', 'about', 'above', 'according', 'accordingly', 'across', 'actually', 'after', 'afterwards', 'again', 'against', 'ain\'t', 'all', 'allow', 'allows', 'almost', 'alone', 'along', 'already', 'also', 'although', 'always', 'am', 'among', 'amongst', 'an', 'and', 'another', 'any', 'anybody', 'anyhow', 'anyone', 'anything', 'anyway', 'anyways', 'anywhere', 'apart', 'appear', 'appreciate', 'appropriate', 'are', 'aren\'t', 'around', 'as', 'aside', 'ask', 'asking', 'associated', 'at', 'available', 'away', 'awfully', 'be', 'became', 'because', 'become', 'becomes', 'becoming', 'been', 'before', 'beforehand', 'behind', 'being', 'believe', 'below', 'beside', 'besides', 'best', 'better', 'between', 'beyond', 'both', 'brief', 'but', 'by', 'c\'mon', 'c\'s', 'came', 'can', 'can\'t', 'cannot', 'cant', 'cause', 'causes', 'certain', 'certainly', 'changes', 'clearly', 'co', 'com', 'come', 'comes', 'concerning', 'consequently', 'consider', 'considering', 'contain', 'containing', 'contains', 'corresponding', 'could', 'couldn\'t', 'course', 'currently', 'definitely', 'described', 'despite', 'did', 'didn\'t', 'different', 'do', 'does', 'doesn\'t', 'doing', 'don\'t', 'done', 'down', 'downwards', 'during', 'each', 'edu', 'eg', 'eight', 'either', 'else', 'elsewhere', 'enough', 'entirely', 'especially', 'et', 'etc', 'even', 'ever', 'every', 'everybody', 'everyone', 'everything', 'everywhere', 'ex', 'exactly', 'example', 'except', 'far', 'few', 'fifth', 'first', 'five', 'followed', 'following', 'follows', 'for', 'former', 'formerly', 'forth', 'four', 'from', 'further', 'furthermore', 'get', 'gets', 'getting', 'given', 'gives', 'go', 'goes', 'going', 'gone', 'got', 'gotten', 'greetings', 'had', 'hadn\'t', 'happens', 'hardly', 'has', 'hasn\'t', 'have', 'haven\'t', 'having', 'he', 'he\'s', 'hello', 'help', 'hence', 'her', 'here', 'here\'s', 'hereafter', 'hereby', 'herein', 'hereupon', 'hers', 'herself', 'hi', 'him', 'himself', 'his', 'hither', 'hopefully', 'how', 'howbeit', 'however', 'i\'d', 'i\'ll', 'i\'m', 'i\'ve', 'ie', 'if', 'ignored', 'immediate', 'in', 'inasmuch', 'inc', 'indeed', 'indicate', 'indicated', 'indicates', 'inner', 'insofar', 'instead', 'into', 'inward', 'is', 'isn\'t', 'it', 'it\'d', 'it\'ll', 'it\'s', 'its', 'itself', 'just', 'keep', 'keeps', 'kept', 'know', 'known', 'knows', 'last', 'lately', 'later', 'latter', 'latterly', 'least', 'less', 'lest', 'let', 'let\'s', 'like', 'liked', 'likely', 'little', 'look', 'looking', 'looks', 'ltd', 'mainly', 'many', 'may', 'maybe', 'me', 'mean', 'meanwhile', 'merely', 'might', 'more', 'moreover', 'most', 'mostly', 'much', 'must', 'my', 'myself', 'name', 'namely', 'nd', 'near', 'nearly', 'necessary', 'need', 'needs', 'neither', 'never', 'nevertheless', 'new', 'next', 'nine', 'no', 'nobody', 'non', 'none', 'noone', 'nor', 'normally', 'not', 'nothing', 'novel', 'now', 'nowhere', 'obviously', 'of', 'off', 'often', 'oh', 'ok', 'okay', 'old', 'on', 'once', 'one', 'ones', 'only', 'onto', 'or', 'other', 'others', 'otherwise', 'ought', 'our', 'ours', 'ourselves', 'out', 'outside', 'over', 'overall', 'own', 'particular', 'particularly', 'per', 'perhaps', 'placed', 'please', 'plus', 'possible', 'presumably', 'probably', 'provides', 'que', 'quite', 'qv', 'rather', 'rd', 're', 'really', 'reasonably', 'regarding', 'regardless', 'regards', 'relatively', 'respectively', 'right', 'said', 'saw', 'say', 'saying', 'says', 'second', 'secondly', 'see', 'seeing', 'seem', 'seemed', 'seeming', 'seems', 'seen', 'self', 'selves', 'sensible', 'sent', 'serious', 'seriously', 'seven', 'several', 'shall', 'she', 'should', 'shouldn\'t', 'since', 'six', 'so', 'some', 'somebody', 'somehow', 'someone', 'something', 'sometime', 'sometimes', 'somewhat', 'somewhere', 'soon', 'sorry', 'specified', 'specify', 'specifying', 'still', 'sub', 'such', 'sup', 'sure', 't\'s', 'take', 'taken', 'tell', 'tends', 'th', 'than', 'thank', 'thanks', 'thanx', 'that', 'that\'s', 'thats', 'the', 'their', 'theirs', 'them', 'themselves', 'then', 'thence', 'there', 'there\'s', 'thereafter', 'thereby', 'therefore', 'therein', 'theres', 'thereupon', 'these', 'they', 'they\'d', 'they\'ll', 'they\'re', 'they\'ve', 'think', 'third', 'this', 'thorough', 'thoroughly', 'those', 'though', 'three', 'through', 'throughout', 'thru', 'thus', 'to', 'together', 'too', 'took', 'toward', 'towards', 'tried', 'tries', 'truly', 'try', 'trying', 'twice', 'two', 'un', 'under', 'unfortunately', 'unless', 'unlikely', 'until', 'unto', 'up', 'upon', 'us', 'use', 'used', 'useful', 'uses', 'using', 'usually', 'value', 'various', 'very', 'via', 'viz', 'vs', 'want', 'wants', 'was', 'wasn\'t', 'way', 'we', 'we\'d', 'we\'ll', 'we\'re', 'we\'ve', 'welcome', 'well', 'went', 'were', 'weren\'t', 'what', 'what\'s', 'whatever', 'when', 'whence', 'whenever', 'where', 'where\'s', 'whereafter', 'whereas', 'whereby', 'wherein', 'whereupon', 'wherever', 'whether', 'which', 'while', 'whither', 'who', 'who\'s', 'whoever', 'whole', 'whom', 'whose', 'why', 'will', 'willing', 'wish', 'with', 'within', 'without', 'won\'t', 'wonder', 'would', 'wouldn\'t', 'yes', 'yet', 'you', 'you\'d', 'you\'ll', 'you\'re', 'you\'ve', 'your', 'yours', 'yourself', 'yourselves', 'zero']))
"""Stopwords based on MySQL <http://dev.mysql.com/doc/refman/5.5/en/fulltext-stopwords.html>"""

# add some of our own stopwords
STOPWORDS.update(u'abcdefghijklmnopqrstuvwxyz')  # single character
STOPWORDS.update([u"'s", u"'ve", u"'re", u"'ll", u"'m", u"'d"])  # after clitics splitting on the above list
STOPWORDS.update([u'ha', u"that'", u'inde', u'definit', u'becaus', u'secondli', u'particularli', u'follow', u'mainli', u'anywai', u'despit', u'onli', u'nobodi', u'sorri', u'elsewher', u'littl', u'clearli', u'late', u'certainli', u'becam', u'els', u'therefor', u"they'v", u'everi', u'nearli', u'thei', u"they'r", u"it'", u'overal', u'ye', u'somebodi', u'therebi', u'veri', u'noon', u'presum', u'truli', u'tri', u'somewher', u"let'", u'anyth', u'thenc', u'possibl', u'necessari', u"t'", u'downward', u'nowher', u'pleas', u"we'r", u"we'v", u'exactli', u'plu', u'realli', u'consequ', u"you'r", u"you'v", u'happen', u'variou', u'whenc', u'concern', u'hopefulli', u'goe', u'someth', u'asid', u'abl', u'tend', u'current', u'rel', u'awai', u'awfulli', u'accord', u'sensibl', u'themselv', u'appreci', u'wa', u'accordingli', u'reason', u'entir', u'believ', u'becom', u'afterward', u'valu', u"i'v", u'actual', u'howev', u'alon', u'unfortun', u'perhap', u'place', u'outsid', u'mani', u'chang', u"here'", u'ourselv', u'onc', u'unlik', u'alreadi', u'formerli', u'respect', u'mere', u'yourselv', u'quit', u'differ', u'describ', u'wai', u"there'", u'hardli', u'sometim', u'sinc', u'everybodi', u'besid', u'immedi', u'dure', u'especi', u'regard', u"c'", u'specifi', u'ani', u"what'", u'whereaft', u'togeth', u'mostli', u'latterli', u'herebi', u'whenev', u'provid', u"he'", u'obvious', u'whatev', u'abov', u'otherwis', u"who'", u'everyon', u'sai', u'give', u'thereaft', u"where'", u'henc', u'anywher', u'indic', u'ar', u'sever', u'probabl', u'everywher', u'alwai', u'cours', u'welcom', u'anoth', u'moreov', u'mayb', u'anyon', u'thoroughli', u'selv', u'noth', u"a'", u'normal', u'wherea', u'someon', u'befor', u'okai', u'consid', u'mai', u'wherev', u'associ', u'avail', u'hereaft', u'appropri', u'furthermor', u'seriou', u'thu', u'correspond', u'greet', u'doe', u'caus', u'meanwhil', u'exampl', u'ignor', u'wherebi', u'thi', u'everyth', u'anybodi', u'usual'])  # stemmed version of the above list

URL_EMAIL_PUNCT = ur'\.\-\_\?\,\'/\\\+\&\%\$#\=~\@\u2019'
"""Set of punctuations that are used by URLs, email addresses and clitics."""

__RE_WHITESPACES__ = re.compile(ur'[\t\n\r\f\v\p{Zs}\p{Zl}\p{Zp}\p{Cc}]+', re.U)
__RE_SEP_AND_PUNCT__ = re.compile(ur'^[\s]+$', re.U | re.V1)
__RE_PUNCTUATION__ = re.compile(ur'^[\p{P}]+$', re.U | re.V1)
__RE_PUNCTUATION2__ = re.compile(ur'[\p{P}--[\p{Pd}\p{Pc}]]', re.U | re.V1)
__RE_SYMBOL__ = re.compile(ur'^[\p{S}]+$', re.U | re.V1)
__RE_NOT_LETTERNUM__ = re.compile(ur'[^\p{L}\p{N}]', re.U | re.V1)
__RE_HYPHENS__ = re.compile(ur'([\p{Pd}\p{Pc}]+)', re.U)

__RE_NUMBERS__ = re.compile(ur'[\+\-\p{Pd}\p{Pc}]?(\d[\d,]*(\.\d+)?)(th|st|nd|rd|k|m|b)?$', re.U | re.I)
__RE_MONEY__ = re.compile(ur'[\+-]?\p{Sc}[\d\.,kmb]+', re.U | re.I)
__RE_CURR_SYMBOL__ = re.compile(ur'^\p{Sc}+$', re.U)  # whole thing is just money
__RE_TIME__ = re.compile(ur'((([0]?[1-9]|1[0-2])([:][0-5][0-9](:[0-5][0-9])?( )?)?(am|pm|a\.m|p\.m))|(([0]?[0-9]|1[0-9]|2[0-3]):[0-5][0-9](:[0-5][0-9])?)|(([0]?[1-9]|1[0-2]|2[0-3]):?[0-5][0-9]))$', re.I | re.U)
__RE_PHONE_NUMBER__ = re.compile(ur'(\(?[2-9]\d\d\)?)?[-\. ]\d{3,4}[-\. ]\d{4}$', re.U)
__RE_URL__ = re.compile(ur'(((http|https|ftp)\://)[a-z0-9\-\.]+\.[a-z]{2,3}(:[a-z0-9]*)?/?([a-z0-9\-\._\?\,\'/\\\+\&\%\$#\=~])*)', re.U | re.I)  # with http/https/ftp
# __RE_URL__ = re.compile(ur'(((http|https|ftp)\://)?[a-z0-9\-\.]+\.[a-z]{2,3}(:[a-z0-9]*)?/?([a-z0-9\-\._\?\,\'/\\\+\&\%\$#\=~])*)', re.U | re.I) # with http/https/ftp
__RE_EMAIL__ = re.compile(ur'.*[0-9a-z][\w_\.-]*\@[0-9a-z][\w_\.-]*\.[a-z]{2,4}.*$', re.I | re.U)

__RE_HAS_CLITIC__ = re.compile(ur'^.+[\'\u2019].*$', re.U)
__RE_CLITICS__ = re.compile(ur'^(.+)([\'\u2019](re|ll|m|ve|s|d))$', re.U | re.I)
__RE_CLITICS2__ = re.compile(ur'^(.+)(s[\'\u2019])$', re.U | re.I)
__RE_NEGATION_CLITIC__ = re.compile(ur'^(.+)(n[\'\u2019]t)$', re.U | re.I)
__RE_TRAILING_QUOTE__ = re.compile(ur'^(.*[^s])([\'\u2019])$', re.U | re.I)

__DEFAULT_NORMALIZE__ = set([u'case', u'consecutive', u'phone', u'time', u'date', u'url', u'email', u'number', u'symbol', u'money', u'hyphen-split', u'clitics-del', u'neg-clitics-keep', u'clitics-normalize'])
"""Default normalization options that are used."""

__DEFAULT_IGNORE_TAGS__ = set(['separator'])
"""By default, separator tags are ignored."""

TAG_EMPTY = 0
"""Tag returned by :meth:`tag_tokens` to mean empty tokens. These tags are ignored by the tokenizer."""

TAG_SEPARATOR = 1
"""Tag returned by :meth:`tag_tokens` to mean separator tokens. These tokens are any standalone character in ``[\\s]`` excluding those defined by `URL_EMAIL_PUNCT`."""

TAG_PUNCT = 2
"""Tag returned by :meth:`tag_tokens` to mean punctuation tokens. Tokens are as defined by Unicode category `P`."""

TAG_SYMBOL = 3
"""Tag returned by :meth:`tag_tokens` to mean symbol tokens. Tokens are as defined by Unicode category `S`."""

TAG_WORD = 4
"""Tag returned by :meth:`tag_tokens` to mean word tokens."""

TAG_NUM = 5
"""Tag returned by :meth:`tag_tokens` to mean number tokens."""

TAG_DATE = 10
"""Tag returned by :meth:`tag_tokens` to mean date tokens."""

TAG_TIME = 11
"""Tag returned by :meth:`tag_tokens` to mean time tokens."""

TAG_PHONE = 20
"""Tag returned by :meth:`tag_tokens` to mean phone number (US) tokens."""

TAG_EMAIL = 21
"""Tag returned by :meth:`tag_tokens` to mean email address tokens."""

TAG_URL = 22
"""Tag returned by :meth:`tag_tokens` to mean URL tokens."""

TAG_MONEY = 30
"""Tag returned by :meth:`tag_tokens` to mean currency related tokens."""


def to_ascii(text):
  """
  Normalizes unicode characters to ASCII, ignoring all errors, using `Unidecode <https://pypi.python.org/pypi/Unidecode>`_ library.

  :param text: text to convert to ASCII.
  :returns: ASCII text.
  """

  import unidecode

  if type(text) != unicode: text = unicode(text, errors='ignore')

  return unidecode.unidecode(text)
#end def


def words(text, normalize=__DEFAULT_NORMALIZE__, ignore_tags=__DEFAULT_IGNORE_TAGS__, filter_stopwords=set(), stemming=False, strip_unicode=False, return_tags=False):
  """
  words(text, strip_unicode=False, normalize=__DEFAULT_NORMALIZE__, ignore_tags=__DEFAULT_IGNORE_TAGS__, filter_stopwords=set(), stemming=False, return_tags=False)

  Tokenize a given string into individual words. This tokenizer is based on a couple of regexes. It first splits a sentence into tokens (based on punctuations, spaces, but not punctuations used in URLs and email addresses), and tries to identify (tag) each token with a category (through :meth:`tag_tokens`), for example url, email, number, etc. One can choose to ignore specific tags by specifying it in the `ignore_tags` argument, and the categories to normalize in the `normalize` argument.

  Besides normalizing for categories, :attr:`normalize` contains options that specify how we deal with punctuation, clitics, negation clitics and case.

  .. _normalization-options:

  Below is a list of available normalization options:

  * ``default``: Include the default set of normalization options. See :attr:`__DEFAULT_NORMALIZE__` for the default settings.
  * ``case``: Convert all tokens to lower case.
  * ``consecutive``: Remove consecutive tags that are of the same kind except consecutive :attr:`TAG_WORD`.
  * ``phone``: Replace phone numbers with ``__PHONE__``.
  * ``time``: Replace simple time expressions with ``__TIME__``.
  * ``url``: Replace URLs with ``__URL__``.
  * ``email``: Replace email addresses with ``__EMAIL__``.
  * ``number``: Replace numbers with ``__NUM__``.
  * ``money``: Replace currency with ``__MONEY__``.
  * ``date``: Replace date tokens with ``__DATE__``.
  * ``punctuation``: Replace punctuations with ``__PUNCT__``.
  * ``symbol``: Replace symbols with ``__SYMBOL__``.
  * ``hyphen-split``: Separate hyphenated tokens into individual tokens. Hyphens are removed in the process.
  * ``hyphen-del``: Remove all hyphens within a single token, i.e merge them into a single token.
  * ``clitics-split``: Separate the clitics part of a token into their own tokens.
  * ``clitics-del``: Remove all non-negation clitics.
  * ``neg-clitics-split``: Separate negation clitics into their own tokens.
  * ``neg-clitics-del``: Remove all negation clitics.

  .. _tag-list:

  The list of tags are (by default all tags except separator are used):

  * ``separator``: Whitespaces (regex ``\\s``) will be tagged with :attr:`TAG_SEPARATOR`. These are ignored by default, and will thus be removed during tokenization.
  * ``punctuation``: Tags punctuations with :attr:`TAG_PUNCT`. When these are ignored, they will be removed during tokenization.
  * ``symbol``: Tags symbols with :attr:`TAG_SYMBOL`. When these are ignored, they will be removed during tokenization.
  * ``phone``: Tag phone numbers with :attr:`TAG_PHONE`.
  * ``time``: Tag time expressions with :attr:`TAG_TIME`.
  * ``date``: Tag date expressions with :attr:`TAG_DATE`. Handles expressions of the form ``(__NUM__)? (a month string) (2 digit __NUM__ starting with 0,1,9 or 4 digit __NUM__ starting with 1, 2)``.
  * ``email``: Tag email addresses with :attr:`TAG_EMAIL`.
  * ``number``: Tag numbers with :attr:`TAG_NUM`.
  * ``url``: Tag URLs with :attr:`TAG_URL`.
  * ``money``: Tag money expressions with :attr:`TAG_MONEY`. Note that this may not work properly if we ignore :attr:`TAG_SYMBOL` tags.

  :param text: the text to tokenize
  :param normalize: specifies how we normalize our tokens. See :ref:`above <normalization-options>` for a list of normalization options.
  :param ignore_tags: specifies the type of category to tag. See :meth:`tag_tokens`.
  :param filter_stopwords: Remove stopwords according to the given list. Defaults to ``None``.
  :param stemming: whether to perform stemming on :attr:`TAG_WORD` tokens.
  :param strip_unicode: normalize unicode characters to pure ascii, ignoring all errors, using the :meth:`to_ascii` method.
  :param return_tags: whether to return list of tags in addition to tokens.

  :returns: a list of tokens if :attr:`return_tags` is ``False``, else returns a list of `(token, tag)`.
  :rtype: list or a list of 2-tuple.

  .. note:: Punctuations are any character that fall in the unicode category of ``P``, except for hyphens, single quotes (see below), and those specified in :attr:`not_punctuation`.
  .. note:: Hyphens are any character that fall in the unicode category of ``Pd`` and ``Pc``.
  .. note:: Clitics are defined as tokens that end with `'re, 'll, 'm, 've, 's, 'd`. The single quote could be ASCII ``'`` or unicode ``\u2019``. Likewise, negation clitics are any tokens that end with `n't`. The single quote could be ASCII ``'`` or unicode ``\u2019``. Also, clitics/hyphenation rules only apply to tokens tagged as :attr:`TAG_WORD`.
  .. note:: For a special token to be normalized (i.e URL, phone, email, etc), it has to be in the tag list, otherwise it is treated as a word and tokenized according to the specified punctuations/hyphenation rules. Likewise, if it is in :attr:`ignore_tags` but not in the :attr:`normalize` list, it would not be subject to clitics/punctuations/hyphenation rules.
  """

  if type(normalize) != set: normalize = set(normalize)
  if u'default' in normalize: normalize.update(__DEFAULT_NORMALIZE__)

  # convert to sets for faster lookups
  normalize = set(normalize)
  ignore_tags = set(ignore_tags)

  tokens = text
  if strip_unicode: tokens = to_ascii(tokens)  # convert unicode text to ascii

  # replace all funny characters with standard empty space
  if u'case' in normalize: tokens = tokens.lower()
  tokens = __RE_WHITESPACES__.sub(u' ', tokens)
  if u'clitics-normalize' in normalize: tokens = tokens.replace(u'\u2019', u'\'')

  # we preliminary split into tokens on spaces and punctuations, but ignoring url/email punctuations and whatever we want to ignore
  tokens = re.split(ur'([[\s\p{P}\p{S}\?]--[' + URL_EMAIL_PUNCT + ur']])', tokens, flags=re.U | re.V1)

  token_tags = zip(tokens, tag_tokens(tokens, ignore_tags=ignore_tags))

  # break apart trailing non letter/numbers
  new_token_tags = []
  for (tok, tag) in token_tags:
    if tag == TAG_WORD:
      # see if there is a punctuation inside here, which will need special processing
      tok = __RE_PUNCTUATION2__.split(tok)
      if len(tok) > 1: new_token_tags.extend(zip(tok, tag_tokens(tok, ignore_tags=ignore_tags)))
      else: new_token_tags.append((tok[0], tag))
    else: new_token_tags.append((tok, tag))
  #end for
  token_tags = new_token_tags

  # now work on hyphens
  if u'hyphen-del' in normalize or u'hyphen-split' in normalize:
    new_token_tags = []
    for (tok, tag) in token_tags:
      if tag == TAG_WORD and __RE_HYPHENS__.search(tok):
        if u'hyphen-del' in normalize:
          tok = __RE_HYPHENS__.sub(u'', tok)
          new_token_tags.append((tok, tag))

        elif u'hyphen-split' in normalize:
          tok = __RE_HYPHENS__.split(tok)
          if len(tok) > 1: new_token_tags.extend(zip(tok, tag_tokens(tok, ignore_tags=ignore_tags)))
      else: new_token_tags.append((tok, tag))
      #end if
    #end for
    token_tags = new_token_tags
  #end if

  # Work on clitics
  if u'clitics-del' in normalize or u'clitics-split' in normalize or u'neg-clitics-split' in normalize or u'neg-clitics-del' in normalize:
    new_token_tags = []
    for (tok, tag) in token_tags:
      if tag == TAG_WORD and __RE_HAS_CLITIC__.match(tok):
        if u'clitics-del' in normalize:
          tok = __RE_CLITICS__.sub(ur'\1', tok)
          tok = __RE_CLITICS2__.sub(ur'\1', tok)

        elif u'clitics-split' in normalize:
          tok = __RE_CLITICS__.sub(ur'\1 \2', tok)
          tok = __RE_CLITICS2__.sub(ur'\1 \2', tok)
        #end if

        if u'neg-clitics-del' in normalize:
          tok = __RE_NEGATION_CLITIC__.sub(ur'\1n', tok)
        elif u'neg-clitics-split' in normalize:
          tok = u' '.join(__RE_NEGATION_CLITIC__.split(tok))

        tok = tok.split()
        new_token_tags.extend(zip(tok, tag_tokens(tok, ignore_tags=ignore_tags)))

      else: new_token_tags.append((tok, tag))
      #end if
    #end for
    token_tags = new_token_tags
  #end if

  # remove empty tags
  token_tags = filter(lambda (tok, tag): tag != TAG_EMPTY, token_tags)
  # print token_tags

  # look for date expressions
  for i, (tok, tag) in enumerate(token_tags):
    if tag == TAG_DATE:
      witness = False

      if i > 0 and token_tags[i-1][1] == TAG_NUM:
        token_tags[i-1] = (token_tags[i-1][0], TAG_DATE)
        witness = True
      #end if
      if i + 1 < len(token_tags) and token_tags[i+1][1] == TAG_NUM:
        token_tags[i+1] = (token_tags[i+1][0], TAG_DATE)
        witness = True
      #end if

      if tok in ['march', 'may'] and not witness:  # grey areas, need confirmation from following or before
        token_tags[i] = (tok, TAG_WORD)
      #end if
    #end if
  #end for

  # look for number expressions separated by PUNCT (, and .)
  # do after consecutive so that numbers are collapsed
  for i, (tok, tag) in enumerate(token_tags):
    if i + 2 < len(token_tags) and tag == TAG_NUM and token_tags[i+1][1] == TAG_PUNCT and token_tags[i+2][1] == TAG_NUM:
      token_tags[i+1] = (token_tags[i+1][1], TAG_NUM)
  # end if

  if u'consecutive' in normalize:
    new_token_tags = []
    prev_tag = None
    for (tok, tag) in token_tags:
      if tag != TAG_WORD and tag == prev_tag: continue

      new_token_tags.append((tok, tag))
      prev_tag = tag
    #end for
    token_tags = new_token_tags
  #end if

  # look for more money expressions
  # do after consecutive so that numbers are collapsed
  for i, (tok, tag) in enumerate(token_tags):
    if i + 1 < len(token_tags) and tag == TAG_SYMBOL and token_tags[i+1][1] == TAG_NUM and __RE_CURR_SYMBOL__.match(tok):
      token_tags[i] = (tok, TAG_MONEY)
      token_tags[i+1] = (token_tags[i+1][1], TAG_MONEY)
    # end if
  # end if

  # remove consecutive MONEY symbols
  if u'consecutive' in normalize:
    new_token_tags = []
    prev_tag = None
    for (tok, tag) in token_tags:
      if tag != TAG_WORD and tag == prev_tag: continue

      new_token_tags.append((tok, tag))
      prev_tag = tag
    #end for
    token_tags = new_token_tags
  #end if

  # tag the special stuffs
  new_token_tags = []
  for (tok, tag) in token_tags:
    if tag == TAG_PHONE and u'phone' in normalize: tok = u'__PHONE__'
    elif tag == TAG_TIME and u'time' in normalize: tok = u'__TIME__'
    elif tag == TAG_EMAIL and u'email' in normalize: tok = u'__EMAIL__'
    elif tag == TAG_DATE and u'date' in normalize: tok = u'__DATE__'
    elif tag == TAG_NUM and u'number' in normalize: tok = u'__NUM__'
    elif tag == TAG_URL and u'url' in normalize: tok = u'__URL__'
    elif tag == TAG_MONEY and u'money' in normalize: tok = u'__MONEY__'
    elif tag == TAG_PUNCT and u'punctuation' in normalize: tok = u'__PUNCT__'
    elif tag == TAG_SYMBOL and u'symbol' in normalize: tok = u'__SYMBOL__'

    new_token_tags.append((tok, tag))
  #end for
  token_tags = new_token_tags

  if filter_stopwords: token_tags = filter(lambda (tok, tag): tok not in filter_stopwords, token_tags)

  if stemming:
    new_token_tags = []
    for (tok, tag) in token_tags:
      if tag == TAG_WORD: tok = stem_tokens(tok)
      new_token_tags.append((tok, tag))
    #end for
    token_tags = new_token_tags
  #end if

  if return_tags: return token_tags

  return map(operator.itemgetter(0), token_tags)
#end def


def tag_tokens(tokens, ignore_tags=__DEFAULT_IGNORE_TAGS__):
  """
  tag_tokens(tokens, ignore_tags=__DEFAULT_IGNORE_TAGS__)

  Given a list of tokens, try to assign a category tag to each of them using prespecified regular expressions. See :meth:`words` for a reference of different tags availables.

  :param tokens: list of tokens to tag.
  :param ignore_tags: Set of tags that we want to ignore. These ignored categories will be tagged as :attr:`TAG_WORD` (default), except for categories of :attr:`TAG_SEPARATOR`, which will be tagged as :attr:`TAG_EMPTY` and subsequently ignored by the tokenizer.

  :return: list of the same length as :attr:`tokens`, containing their corresponding tags for each token.
  :rtype: list.
  """

  tags = [TAG_WORD for i in xrange(len(tokens))]

  for i, tok in enumerate(tokens):
    if not tok: tags[i] = TAG_EMPTY
    if __RE_SEP_AND_PUNCT__.match(tok): tags[i] = TAG_EMPTY if u'separator' in ignore_tags else TAG_SEPARATOR
    if __RE_PUNCTUATION__.match(tok): tags[i] = TAG_EMPTY if u'punctuation' in ignore_tags else TAG_PUNCT
    if __RE_SYMBOL__.match(tok): tags[i] = TAG_EMPTY if u'symbol' in ignore_tags else TAG_SYMBOL
  #end for

  if u'phone' not in ignore_tags:
    for i, tok in enumerate(tokens):
      if __RE_PHONE_NUMBER__.match(tok): tags[i] = TAG_PHONE

  if u'time' not in ignore_tags:
    for i, tok in enumerate(tokens):
      if __RE_TIME__.match(tok): tags[i] = TAG_TIME

  if u'date' not in ignore_tags:
    for i, tok in enumerate(tokens):
      if tok in MONTHS: tags[i] = TAG_DATE

  if u'email' not in ignore_tags:
    for i, tok in enumerate(tokens):
      if __RE_EMAIL__.match(tok): tags[i] = TAG_EMAIL

  if u'number' not in ignore_tags:
    for i, tok in enumerate(tokens):
      if __RE_NUMBERS__.match(tok): tags[i] = TAG_NUM

  if u'url' not in ignore_tags:
    for i, tok in enumerate(tokens):
      if __RE_URL__.match(tok): tags[i] = TAG_URL

  if u'money' not in ignore_tags:
    for i, tok in enumerate(tokens):
      if __RE_MONEY__.match(tok): tags[i] = TAG_MONEY

  return tags
#end def


def words_in_sentences(sents, normalize=__DEFAULT_NORMALIZE__, ignore_tags=__DEFAULT_IGNORE_TAGS__, filter_stopwords=set(), stemming=False, strip_unicode=False, return_tags=False):
  """
  words_in_sentences(sents, strip_unicode=False, normalize=__DEFAULT_NORMALIZE__, ignore_tags=__DEFAULT_IGNORE_TAGS__, filter_stopwords=set(), return_tags=False)

  Tokenize by words a list of strings (each string item is like a sentence). Empty sentences will be removed.

  :param sents: a list of string.

  The remaining parameters are same as that for :meth:`words`.

  :returns: a list of list of tokens or (list of tokens, list of tags)."""

  return filter(lambda sent: len(sent) > 0, map(lambda sent: words(sent, strip_unicode=strip_unicode, normalize=normalize, ignore_tags=ignore_tags, filter_stopwords=filter_stopwords, stemming=stemming, return_tags=return_tags), sents))
#end def

import splitta.sbd
__SPLITTA_MODEL__ = None
"""Splitta model that will be loaded on first use."""


def sentences(text, split_paragraph=True):
  """
  Interface to `Splitta <http://code.google.com/p/splitta/>`_ to tokenize sentences.

  .. note:: In first call, the Splitta naive bayes model will be loaded. It takes about 10+ seconds to load the model on cab head node.

  :param text: text to tokenize by sentences.
  :param split_paragraph: split text by paragraphs first (i.e splitting on instances of ``\\n\\n``).
  :returns: list of sentences.
  :rtype: list.
  """

  global __SPLITTA_MODEL__

  # assumes splitta model is in the same directory as this script
  if not __SPLITTA_MODEL__: __SPLITTA_MODEL__ = splitta.sbd.load_sbd_model(os.path.dirname(__file__) + u'/splitta/model_nb/', use_svm=False)

  # split up text by paragraphs first
  if split_paragraph:
    sents = []
    for p in text.split('\n\n'):
      if p.endswith('u.'): p = p[:-2] + 'u .'
      sents.extend(splitta.sbd.sbd_text(__SPLITTA_MODEL__, p, do_tok=False))
  else:
    if text.endswith('u.'): text = text[:-2] + 'u .'
    sents = splitta.sbd.sbd_text(__SPLITTA_MODEL__, text, do_tok=False)

  return filter(lambda sent: len(sent) > 0, sents)
#end def


def filter_stopwords(tokens, my_stopwords=STOPWORDS):
  """
  filter_stopwords(tokens, my_stopwords=STOPWORDS)

  Filter stopwords from a list of tokens.

  :param tokens: a list of tokens.
  :param my_stopwords: option to use your own stopwords list. Defaults to MySQL stopwords found at `http://dev.mysql.com/doc/refman/5.5/en/fulltext-stopwords.html <http://dev.mysql.com/doc/refman/5.5/en/fulltext-stopwords.html>`_.
  :returns: a list of tokens with stopwords removed.
  :rtype: list"""

  return filter(lambda w: w not in my_stopwords, tokens)
#end def


def ngram_tokens(tokens, n, sep_char=u'_', filter_stopwords=STOPWORDS):
  """
  Generate list of n-grams from a sequence of tokens. N-gram tokens do not start/end with stopwords define in :attr:`filter_stopwords`.

  :param tokens: a sequence of tokens.
  :param n: the number of tokens to conjoin together. If given a list, n-grams are computed for each `n` value in turn.
  :param sep_char: separation character to use between consecutive tokens.
  :param filter_stopwords: Remove stopwords according to the given list. Defaults to stopword list defined by :attr:`STOPWORDS`.

  :returns: a list of n-gram tokens.
  :rtype: list
  """

  ngrams = []
  n_counts = n if isinstance(n, list) else [n]

  for i, tok in enumerate(tokens):
    if tok in filter_stopwords: continue  # cannot start ngram with stopwords

    for k in n_counts:
      e = i + k
      if e > len(tokens): continue
      if tokens[e-1] in filter_stopwords: continue  # cannot end ngram with stopwords
      ngrams.append(sep_char.join(tokens[i:e]))
    #end for
  #end for

  return ngrams
#end def

stemmer = PorterStemmer()


def stem_tokens(tokens):
  """
  Apply the Porter stemmer to a list of tokens.

  This is based on the Python implementation of Porter stemmer by Vivake Gupta `http://tartarus.org/martin/PorterStemmer/python.txt <http://tartarus.org/martin/PorterStemmer/python.txt>`_.

  :param tokens: a sequence of tokens or a single token.
  :returns: a corresponding list of Porter steemed tokens.
  :rtype: list
  """

  if type(tokens) == list: return map(lambda w: stemmer.stem(w, 0, len(w)-1), tokens)
  return stemmer.stem(tokens, 0, len(tokens)-1)
#end def
