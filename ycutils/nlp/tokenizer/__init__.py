"""
******************
*tokenizer* module
******************

This module contains function to split a piece of text into individual sentences (using `Splitta <http://code.google.com/p/splitta/>`_) and into individual words (using many regexes).
This module works in a 2-step pipeline fashion. The first step is tagging, which involves using :class:`ycutils.nlp.tokenizer.Taggers` objects to identify sequences of characters as "tokens".
The second step is normalization, which performs normalization actions on sequence of tokens returned in the previous step. Normalization actions could be like lower casing, stemming, etc.

Sample text:
  `At 2PM today, In fairness, the national networks did show 74,730st other images, especially shots of Lake Pontchartrain spilling out onto Lakeshore Drive or traffic signs twisting in the wind, but there was no information about when the images were recorded. Is this what the wind is like now? Three hours ago? Or as far back as yesterday? I can watch the same sports highlights on ESPN over and over again and never be bored by the repetition. The difference here, though, is that I know that Lebron's blocked shot, Drew's pinpoint touchdown pass, Tiger's escape from the bunker isn't still happening. But watching the Weather Channel hour after hour - and what else does one concerned about the city do but that? - one has a hard time separating the past from the present, distinguishing the weather that was from the weather that is. You'd also have a hard time separating flooding that's perilous from the flooding that's inconvenient. What do these reporters mean when they say it's flooding? Is it water up to the headlights or water into raised houses? Water that people cross with rubber boots? Or water requiring a pirogue? Is it an amount we see every few years or an amount we see every Wednesday? So long as it wasn't an amount caused by busted levees I knew the city would survive. Jarvis DeBerry can be reached at jdeberry@timespicayune.com or (504) 826.3355. Follow him at http://connect.nola.com/user/jdeberry/index.html and at twitter.com/jerry.`

Example:

.. code-block:: python

   import ycutils.nlp.tokenizer

   toknzr = ycutils.nlp.tokenizer.Tokenizer()
   sents = toknzr.sentences(text)
   tokens = toknzr.words_in_sentences(sents)


*Tokenizer* class
=================
.. autoclass:: ycutils.nlp.tokenizer.Tokenizer
  :members:
"""

import operator
import os

import splitta.sbd

import normalizers
import taggers

SPLITTA_MODEL = None


class Tokenizer:
  """
  This class sets up the environment for performing tokenizations.

  :param dict configuration: a dictionary containing two keys: `taggers` and `normalizers`, whose values are list of identifiers for tagging and normalization. Identifiers can be specified using its `NAME`, or class, or instantiation.
  """

  DEFAULT_CONFIG = {
      'taggers': ['email', 'url', 'phone-number', 'acronym', 'money', 'number', 'clitics', 'hyphenated-words', 'punctuation', 'non-whitespace'],
      'normalizers': ['url', 'email', 'phone-number', 'hyphens', 'clitics', 'number', 'punctuation', 'case', 'porter-stemmer', 'stopword', 'consecutive', 'unicode']
  }
  """The default configuration dictionary for tokenizer."""

  def __init__(self, configuration=DEFAULT_CONFIG):
    self.configuration(configuration)
  #end def

  def configuration(self, configuration=None):
    """
    Sets the configuration for this tokenizer.

    :param dict configuration: a dictionary containing two keys: `taggers` and `normalizers`, whose values are list of identifiers for tagging and normalization. Identifiers can be specified using its `NAME`, or class, or instantiation.
    """

    if not configuration: return self.configuration_

    self.configuration_ = {'taggers': [], 'normalizers': []}

    for tagger in configuration.get('taggers', []):
      if isinstance(tagger, taggers.Tagger): self.configuration_['taggers'].append(tagger)
      elif type(tagger) is type: self.configuration_['taggers'].append(tagger())
      elif isinstance(tagger, basestring): self.configuration_['taggers'].append(taggers.get_tagger(tagger))
    #end for

    for normalizer in configuration.get('normalizers', []):
      if isinstance(normalizer, normalizers.Normalizer): self.configuration_['normalizers'].append(normalizer)
      elif type(normalizer) is type: self.configuration_['normalizers'].append(normalizer())
      elif isinstance(normalizer, basestring): self.configuration_['normalizers'].append(normalizers.get_normalizer(normalizer))
    #end for
  #end def

  def words(self, sent, return_tags=False):
    """
    Treats the string :attr:`sent` as a sentence and tokenizes it.

    :param string sent: sentence string to tokenize.
    :param bool return_tags: whether to return list of tags in addition to tokens.

    :returns: tokens if :attr:`return_tags` is False and list of (tag, token) otherwise.
    :rtypes: list.
    """

    assert isinstance(sent, basestring)
    tag_ranges = []

    for tagger in self.configuration_['taggers']:
      start = 0
      ranges = []

      for tagger_id, tag_start, tag_end in tag_ranges:
        if tag_start < start: raise ValueError('There should not be overlapping ranges!')
        ranges += [(tagger.ID, s+start, e+start) for s, e in tagger.tag(sent[start:tag_start])]
        start = tag_end
      #end for

      ranges += [(tagger.ID, s+start, e+start) for s, e in tagger.tag(sent[start:len(sent)])]
      tag_ranges += ranges

      tag_ranges.sort(key=lambda (id, s, e): s)
    #end for

    tokens = [(tagger_id, sent[s:e]) for tagger_id, s, e in tag_ranges]
    normalized_tokens = reduce(lambda normalized, normalizer: normalizer.normalize(normalized), self.configuration_['normalizers'], tokens)

    if return_tags: return filter(lambda (tagger_id, tok): tok, normalized_tokens)

    return map(operator.itemgetter(1), filter(lambda (tagger_id, tok): tok, normalized_tokens))
  #end def

  def sentences(self, text, split_paragraph=True):
    """
    Interface to `Splitta <http://code.google.com/p/splitta/>`_ to tokenize sentences.

    .. note:: In first call, the Splitta naive bayes model will be loaded. It takes about 10+ seconds to load the model on cab head node.

    :param string text: text to tokenize by sentences.
    :param bool split_paragraph: split text by paragraphs first (i.e splitting on instances of ``\\n\\n``).
    :returns: list of sentences.
    :rtype: list.
    """

    global SPLITTA_MODEL

    # assumes splitta model is in the same directory as this script
    if SPLITTA_MODEL is None: SPLITTA_MODEL = splitta.sbd.load_sbd_model(os.path.dirname(__file__) + u'/splitta/model_nb/', use_svm=False)

    # split up text by paragraphs first
    if split_paragraph:
      sents = []
      for p in text.split('\n\n'):
        if p.endswith('u.'): p = p[:-2] + 'u .'
        sents.extend(splitta.sbd.sbd_text(SPLITTA_MODEL, p, do_tok=False))
    else:
      if text.endswith('u.'): text = text[:-2] + 'u .'
      sents = splitta.sbd.sbd_text(SPLITTA_MODEL, text, do_tok=False)

    return filter(lambda sent: len(sent) > 0, sents)
  #end def

  def words_in_sentences(self, sents, return_tags=False):
    """
    Tokenize by words a list of strings (each string item is like a sentence). Empty sentences will be removed.

    :param list sents: a list of string.
    :param bool return_tags: whether to return list of tags in addition to tokens.

    :returns: list of sentences containing list of tokens.
    :rtype: list of list.
    """

    return filter(lambda tokens: len(tokens) > 0, [self.words(sent, return_tags) for sent in sents])
  #end def

  def tokenize_text(self, text, split_paragraph=True, return_tags=False):
    """
    Splits a bunch of text into sentences and then tokenizes it.

    :param string text: text to sentence split and tokenize.
    :param bool split_paragraph: whether to split `\\n\\n` sequences into separate sentences.
    :param bool return_tags: whether to return list of tags in addition to tokens.

    :returns: list of sentences containing list of tokens.
    :rtype: list of list.
    """

    sents = self.sentences(text, split_paragraph=split_paragraph)
    return filter(lambda tokens: len(tokens) > 0, [self.words(sent, return_tags) for sent in sents])
  #end def
#end class


if __name__ == '__main__':
  tokenizer = Tokenizer()

  print tokenizer.tokenize_text(u'this IS a hello world string with hello worlds\' can\'t and 78 hundred thousand dollars.  This is the next sentence.')
#end if
