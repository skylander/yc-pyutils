"""
********************
*normalizers* module
********************

Normalizers are the second stage of tokenization. They make use of :class:`ycutils.nlp.tokenizer.normalizers.Normalizer` objects which contain the :meth:`ycutils.nlp.tokenizer.normalizers.Normalizer.normalize`. See :class:`ycutils.nlp.tokenizer.normalizers.Normalizer` for more details.

Base normalizers
================
.. autoclass:: ycutils.nlp.tokenizer.normalizers.Normalizer
  :members:

.. autoclass:: ycutils.nlp.tokenizer.normalizers.TokenNormalizer
  :members:

Implemented taggers
===================
.. autoclass:: ycutils.nlp.tokenizer.normalizers.ConsecutiveNormalizer
  :members: NAME
  :undoc-members:

.. autoclass:: ycutils.nlp.tokenizer.normalizers.CaseNormalizer
  :members: NAME
  :undoc-members:

.. autoclass:: ycutils.nlp.tokenizer.normalizers.HyphensNormalizer
  :members: NAME
  :undoc-members:

.. autoclass:: ycutils.nlp.tokenizer.normalizers.CliticsNormalizer
  :members: NAME
  :undoc-members:

.. autoclass:: ycutils.nlp.tokenizer.normalizers.PorterStemmerNormalizer
  :members: NAME

.. autoclass:: ycutils.nlp.tokenizer.normalizers.NumberNormalizer
  :members: NAME
  :undoc-members:

.. autoclass:: ycutils.nlp.tokenizer.normalizers.PunctuationNormalizer
  :members: NAME
  :undoc-members:

.. autoclass:: ycutils.nlp.tokenizer.normalizers.URLNormalizer
  :members: NAME
  :undoc-members:

.. autoclass:: ycutils.nlp.tokenizer.normalizers.EmailNormalizer
  :members: NAME
  :undoc-members:

.. autoclass:: ycutils.nlp.tokenizer.normalizers.PhoneNumberNormalizer
  :members: NAME
  :undoc-members:

.. autoclass:: ycutils.nlp.tokenizer.normalizers.StopwordNormalizer
  :members: NAME, STOPWORDS_
  :undoc-members:

.. autoclass:: ycutils.nlp.tokenizer.normalizers.UnicodeNormalizer
  :members: NAME
  :undoc-members:
"""

import operator
import regex as re

import unidecode
import taggers

NORMALIZER_NAMES = {}
NORMALIZER_IDS = {}


def register_normalizer(normalizer):
  """
  Decorator function for registering a normalizer in the normalizer database. This is useful when you want to access the normalizer by its name instead of instantiating it.
  """

  if normalizer.NAME in NORMALIZER_NAMES: raise KeyError('Normalizer name \'{}\' already exists.'.format(normalizer.NAME))
  if normalizer.ID in NORMALIZER_IDS: raise KeyError('Normalizer ID \'{}\' already exists.'.format(normalizer.ID))

  NORMALIZER_NAMES[normalizer.NAME] = normalizer
  NORMALIZER_IDS[normalizer.ID] = normalizer

  return normalizer
#end def


def get_normalizer(name):
  """Instantiates a :class:`ycutils.nlp.tokenizer.normalizers.Normalizer` object by its name."""
  return NORMALIZER_NAMES[name]()
#end def


class Normalizer(object):
  """
  Normalizer objects are responsible for performing actions on sequences of tagged tokens.
  They contain the :meth:`normalize` method which takes a sequence of (tag, token) tokens and returned a normalized list.
  """

  NAME = 'unnamed'
  """The name of the normalizer."""

  ID = 0
  """A unique ID for the normalizer. Convention is to use 1-10 for basic normalizers, 10-99 for everything else, and >= 100 for user-defined normalizers not found in this library."""

  def __init__(self): pass

  def normalize(self, tokens):
    """
    Heart and soul of a normalizer. Takes a sequence of tagged tokens (i.e tuples) and returns a corresponding list of tuples with the token value ``normalized``.

    :param list sent: a list of tuples `(tagger_id, token)`.
    :returns: list of tuples `(tagger_id, normalized_token)`.
    :rtype: list of tuples.
    """
    return map(operator.itemgetter(1), tokens)
  #end def
#end class


class TokenNormalizer(Normalizer):
  """
  A base normalizer that acts on a single token at a time (instead of an entire list like :class:`ycutils.nlp.tokenizer.normalizers.Normalizer`).

  :param iterable applies_to: list of tagger ID that the normalizer will be applied to (the others will be ignored).
  :param str normalize_text: if this value is set, :meth:`normalize_token` will automatically set token value to it.
  :param func normalize_func: if this value is set, :meth:`normalize_token` will set token value to token value returned by `normalize_func`. `normalize_func` is a function that takes a single token argument.
  """

  NAME = 'token'
  ID = 1

  def __init__(self, applies_to=None, normalize_text=None, normalize_func=None):
    self.applies_to_ = set(applies_to) if applies_to else None
    self.normalize_text_ = normalize_text
    self.normalize_func_ = normalize_func
  #end def

  def normalize(self, tokens):
    normalized_tokens = []
    for tagger_id, tok in tokens:
      normalized = self.normalize_token(tagger_id, tok) if not self.applies_to_ or tagger_id in self.applies_to_ else (tagger_id, tok)

      if isinstance(normalized, tuple): normalized_tokens.append(normalized)
      else: normalized_tokens += normalized
    #end for

    return normalized_tokens
  #end def

  def normalize_token(self, tagger_id, token):
    """
    Override this method with your own single (tagger, token) normalizing function.
    """

    if self.normalize_text_ is not None: return (tagger_id, self.normalize_text_)
    if self.normalize_func_ is not None: return (tagger_id, self.normalize_func_(token))

    return (tagger_id, token)
  #end def
#end class


@register_normalizer
class ConsecutiveNormalizer(Normalizer):
  """Removes consecutive tokens that are tagged as the same type and contain the same value."""

  NAME = 'consecutive'
  ID = 3

  def normalize(self, tokens):
    normalized_tokens = []
    prev_id, prev_token = None, None
    for tagger_id, token in tokens:
      if (tagger_id, token) == (prev_id, prev_token): continue

      normalized_tokens.append((tagger_id, token))
      prev_id, prev_token = tagger_id, token
    #end for

    return normalized_tokens
  #end def
#end class


@register_normalizer
class CaseNormalizer(TokenNormalizer):
  """Normalizes the tokens to lower case."""

  NAME = 'case'
  ID = 10

  def __init__(self): super(CaseNormalizer, self).__init__()

  def normalize_token(self, tagger_id, tok):
    if tok.startswith('__') and tok.endswith('__'): return (tagger_id, tok)

    return (tagger_id, tok.lower())
  #end def
#end class


@register_normalizer
class HyphensNormalizer(TokenNormalizer):
  """
  Normalize hyphenated words.

  :param bool delete_hyphens: removes all hyphens in token.
  :param bool normalize_hyphens: normalizes all hyphens in token to `-` (ascii).
  """

  NAME = 'hyphens'
  ID = 11

  def __init__(self, delete_hyphens=True, normalize_hyphens=True):
    self.delete_hyphens_ = delete_hyphens
    self.normalize_hyphens_ = normalize_hyphens
    super(HyphensNormalizer, self).__init__(applies_to=[taggers.get_tagger('hyphenated-words').ID])
  #end def

  def normalize_token(self, tagger_id, tok):
    if self.delete_hyphens_: tok = re.sub(ur'[\p{Pd}\p{Pc}]+', '', tok, flags=re.U)
    elif self.normalize_hyphens_: tok = re.subn(ur'[\p{Pd}\p{Pc}]', '-', tok, flags=re.U)[0]

    return (tagger_id, tok)
  #end def
#end class


@register_normalizer
class CliticsNormalizer(Normalizer):
  """
  Normalize clitics.

  :param bool delete_apostrophe: removes the apostrophe character in token.
  :param bool normalize_clitics: normalizes all apostrophes in token to `'` (ascii).
  :param bool split_clitics: split token into its word and clitic.
  """

  NAME = 'clitics'
  ID = 12

  def __init__(self, delete_apostrophe=True, split_clitics=False, normalize_clitics=True):
    self.delete_apostrophe_ = delete_apostrophe
    self.normalize_clitics_ = normalize_clitics
    self.split_clitics_ = split_clitics
  #end def

  def normalize(self, tokens):
    clitic_tagger_id = taggers.get_tagger('clitics').ID
    normalized_tokens = []

    for tagger_id, tok in tokens:
      if tagger_id == clitic_tagger_id:
        if self.normalize_clitics_: tok = re.subn(ur'[\'\u2019]', u'\'', tok, flags=re.U)[0]

        if self.split_clitics_:
          m = re.match(ur'(.+)([\'\u2019](re|ll|m|ve|s|d)|s[\'\u2019]|n[\'\u2019]t)$', tok, re.U)
          if m:
            normalized_tokens.append((tagger_id, m.group(1)))
            tok = m.group(2)
          #end if
        #end if

        if self.delete_apostrophe_: tok = re.subn(ur'[\'\u2019]', '', tok, flags=re.U)[0]
      #end if

      normalized_tokens.append((tagger_id, tok))
    #end for

    return normalized_tokens
  #end def
#end class


@register_normalizer
class NumberNormalizer(TokenNormalizer):
  """Normalizes numerical tokens."""

  NAME = 'number'
  ID = 13

  def __init__(self, normalize_text=u'__NUMBER__'): super(NumberNormalizer, self).__init__(applies_to=[taggers.get_tagger('number').ID], normalize_text=normalize_text)
#end class


@register_normalizer
class PunctuationNormalizer(TokenNormalizer):
  """Normalizes punctuation tokens."""

  NAME = 'punctuation'
  ID = 14

  def __init__(self, normalize_text=u'__PUNCT__'): super(PunctuationNormalizer, self).__init__(applies_to=[taggers.get_tagger('punctuation').ID], normalize_text=normalize_text)
#end class


@register_normalizer
class PorterStemmerNormalizer(TokenNormalizer):
  """Normalizes tokens using PorterStemmer."""

  from PorterStemmer import PorterStemmer
  stemmer_ = PorterStemmer()

  NAME = 'porter-stemmer'
  ID = 15

  def __init__(self): super(PorterStemmerNormalizer, self).__init__(applies_to=[taggers.get_tagger('non-whitespace').ID, taggers.get_tagger('hyphenated-words').ID, taggers.get_tagger('clitics').ID])

  def normalize_token(self, tagger_id, token): return (tagger_id, self.stemmer_.stem(token, 0, len(token)-1))
#end class


@register_normalizer
class URLNormalizer(TokenNormalizer):
  """Normalizes URL tokens."""

  NAME = 'url'
  ID = 20

  def __init__(self, normalize_text=u'__URL__'): super(URLNormalizer, self).__init__(applies_to=[taggers.get_tagger('url').ID], normalize_text=normalize_text)
#end class


@register_normalizer
class EmailNormalizer(TokenNormalizer):
  """Normalizes email tokens."""

  NAME = 'email'
  ID = 21

  def __init__(self, normalize_text=u'__EMAIL__'): super(EmailNormalizer, self).__init__(applies_to=[taggers.get_tagger('email').ID], normalize_text=normalize_text)
#end class


@register_normalizer
class PhoneNumberNormalizer(TokenNormalizer):
  """Normalizes phone number tokens."""

  NAME = 'phone-number'
  ID = 22

  def __init__(self, normalize_text=u'__PHONE__'): super(PhoneNumberNormalizer, self).__init__(applies_to=[taggers.get_tagger('phone-number').ID], normalize_text=normalize_text)
#end class


@register_normalizer
class StopwordNormalizer(TokenNormalizer):
  """
  Normalizes stopword tokens. Note that it only applies to :class:`ycutils.nlp.tokenizer.taggers.NonWhitespaceTagger`, :class:`ycutils.nlp.tokenizer.taggers.HyphenatedWordsTagger` and :class:`ycutils.nlp.tokenizer.taggers.CliticsTagger` tokens (any other types of tagged tokens are ignored by this normalizer).

  :param set stopwords: set of stopwords to use.
  :param set additional_stopwords: additional stopwords to use beyond the provided defaults.
  """

  NAME = 'stopword'
  ID = 23

  STOPWORDS_ = set(map(unicode, ['a', 'a\'s', 'able', 'about', 'above', 'according', 'accordingly', 'across', 'actually', 'after', 'afterwards', 'again', 'against', 'ain\'t', 'all', 'allow', 'allows', 'almost', 'alone', 'along', 'already', 'also', 'although', 'always', 'am', 'among', 'amongst', 'an', 'and', 'another', 'any', 'anybody', 'anyhow', 'anyone', 'anything', 'anyway', 'anyways', 'anywhere', 'apart', 'appear', 'appreciate', 'appropriate', 'are', 'aren\'t', 'around', 'as', 'aside', 'ask', 'asking', 'associated', 'at', 'available', 'away', 'awfully', 'be', 'became', 'because', 'become', 'becomes', 'becoming', 'been', 'before', 'beforehand', 'behind', 'being', 'believe', 'below', 'beside', 'besides', 'best', 'better', 'between', 'beyond', 'both', 'brief', 'but', 'by', 'c\'mon', 'c\'s', 'came', 'can', 'can\'t', 'cannot', 'cant', 'cause', 'causes', 'certain', 'certainly', 'changes', 'clearly', 'co', 'com', 'come', 'comes', 'concerning', 'consequently', 'consider', 'considering', 'contain', 'containing', 'contains', 'corresponding', 'could', 'couldn\'t', 'course', 'currently', 'definitely', 'described', 'despite', 'did', 'didn\'t', 'different', 'do', 'does', 'doesn\'t', 'doing', 'don\'t', 'done', 'down', 'downwards', 'during', 'each', 'edu', 'eg', 'eight', 'either', 'else', 'elsewhere', 'enough', 'entirely', 'especially', 'et', 'etc', 'even', 'ever', 'every', 'everybody', 'everyone', 'everything', 'everywhere', 'ex', 'exactly', 'example', 'except', 'far', 'few', 'fifth', 'first', 'five', 'followed', 'following', 'follows', 'for', 'former', 'formerly', 'forth', 'four', 'from', 'further', 'furthermore', 'get', 'gets', 'getting', 'given', 'gives', 'go', 'goes', 'going', 'gone', 'got', 'gotten', 'greetings', 'had', 'hadn\'t', 'happens', 'hardly', 'has', 'hasn\'t', 'have', 'haven\'t', 'having', 'he', 'he\'s', 'hello', 'help', 'hence', 'her', 'here', 'here\'s', 'hereafter', 'hereby', 'herein', 'hereupon', 'hers', 'herself', 'hi', 'him', 'himself', 'his', 'hither', 'hopefully', 'how', 'howbeit', 'however', 'i\'d', 'i\'ll', 'i\'m', 'i\'ve', 'ie', 'if', 'ignored', 'immediate', 'in', 'inasmuch', 'inc', 'indeed', 'indicate', 'indicated', 'indicates', 'inner', 'insofar', 'instead', 'into', 'inward', 'is', 'isn\'t', 'it', 'it\'d', 'it\'ll', 'it\'s', 'its', 'itself', 'just', 'keep', 'keeps', 'kept', 'know', 'known', 'knows', 'last', 'lately', 'later', 'latter', 'latterly', 'least', 'less', 'lest', 'let', 'let\'s', 'like', 'liked', 'likely', 'little', 'look', 'looking', 'looks', 'ltd', 'mainly', 'many', 'may', 'maybe', 'me', 'mean', 'meanwhile', 'merely', 'might', 'more', 'moreover', 'most', 'mostly', 'much', 'must', 'my', 'myself', 'name', 'namely', 'nd', 'near', 'nearly', 'necessary', 'need', 'needs', 'neither', 'never', 'nevertheless', 'new', 'next', 'nine', 'no', 'nobody', 'non', 'none', 'noone', 'nor', 'normally', 'not', 'nothing', 'novel', 'now', 'nowhere', 'obviously', 'of', 'off', 'often', 'oh', 'ok', 'okay', 'old', 'on', 'once', 'one', 'ones', 'only', 'onto', 'or', 'other', 'others', 'otherwise', 'ought', 'our', 'ours', 'ourselves', 'out', 'outside', 'over', 'overall', 'own', 'particular', 'particularly', 'per', 'perhaps', 'placed', 'please', 'plus', 'possible', 'presumably', 'probably', 'provides', 'que', 'quite', 'qv', 'rather', 'rd', 're', 'really', 'reasonably', 'regarding', 'regardless', 'regards', 'relatively', 'respectively', 'right', 'said', 'saw', 'say', 'saying', 'says', 'second', 'secondly', 'see', 'seeing', 'seem', 'seemed', 'seeming', 'seems', 'seen', 'self', 'selves', 'sensible', 'sent', 'serious', 'seriously', 'seven', 'several', 'shall', 'she', 'should', 'shouldn\'t', 'since', 'six', 'so', 'some', 'somebody', 'somehow', 'someone', 'something', 'sometime', 'sometimes', 'somewhat', 'somewhere', 'soon', 'sorry', 'specified', 'specify', 'specifying', 'still', 'sub', 'such', 'sup', 'sure', 't\'s', 'take', 'taken', 'tell', 'tends', 'th', 'than', 'thank', 'thanks', 'thanx', 'that', 'that\'s', 'thats', 'the', 'their', 'theirs', 'them', 'themselves', 'then', 'thence', 'there', 'there\'s', 'thereafter', 'thereby', 'therefore', 'therein', 'theres', 'thereupon', 'these', 'they', 'they\'d', 'they\'ll', 'they\'re', 'they\'ve', 'think', 'third', 'this', 'thorough', 'thoroughly', 'those', 'though', 'three', 'through', 'throughout', 'thru', 'thus', 'to', 'together', 'too', 'took', 'toward', 'towards', 'tried', 'tries', 'truly', 'try', 'trying', 'twice', 'two', 'un', 'under', 'unfortunately', 'unless', 'unlikely', 'until', 'unto', 'up', 'upon', 'us', 'use', 'used', 'useful', 'uses', 'using', 'usually', 'value', 'various', 'very', 'via', 'viz', 'vs', 'want', 'wants', 'was', 'wasn\'t', 'way', 'we', 'we\'d', 'we\'ll', 'we\'re', 'we\'ve', 'welcome', 'well', 'went', 'were', 'weren\'t', 'what', 'what\'s', 'whatever', 'when', 'whence', 'whenever', 'where', 'where\'s', 'whereafter', 'whereas', 'whereby', 'wherein', 'whereupon', 'wherever', 'whether', 'which', 'while', 'whither', 'who', 'who\'s', 'whoever', 'whole', 'whom', 'whose', 'why', 'will', 'willing', 'wish', 'with', 'within', 'without', 'won\'t', 'wonder', 'would', 'wouldn\'t', 'yes', 'yet', 'you', 'you\'d', 'you\'ll', 'you\'re', 'you\'ve', 'your', 'yours', 'yourself', 'yourselves', 'zero']))
  """Stopwords based on MySQL <http://dev.mysql.com/doc/refman/5.5/en/fulltext-stopwords.html>"""

  # add some of our own stopwords
  STOPWORDS_.update(u'abcdefghijklmnopqrstuvwxyz')  # single character
  STOPWORDS_.update([u"'s", u"'ve", u"'re", u"'ll", u"'m", u"'d"])  # after clitics splitting on the above list
  STOPWORDS_.update([u'ha', u"that'", u'inde', u'definit', u'becaus', u'secondli', u'particularli', u'follow', u'mainli', u'anywai', u'despit', u'onli', u'nobodi', u'sorri', u'elsewher', u'littl', u'clearli', u'late', u'certainli', u'becam', u'els', u'therefor', u"they'v", u'everi', u'nearli', u'thei', u"they'r", u"it'", u'overal', u'ye', u'somebodi', u'therebi', u'veri', u'noon', u'presum', u'truli', u'tri', u'somewher', u"let'", u'anyth', u'thenc', u'possibl', u'necessari', u"t'", u'downward', u'nowher', u'pleas', u"we'r", u"we'v", u'exactli', u'plu', u'realli', u'consequ', u"you'r", u"you'v", u'happen', u'variou', u'whenc', u'concern', u'hopefulli', u'goe', u'someth', u'asid', u'abl', u'tend', u'current', u'rel', u'awai', u'awfulli', u'accord', u'sensibl', u'themselv', u'appreci', u'wa', u'accordingli', u'reason', u'entir', u'believ', u'becom', u'afterward', u'valu', u"i'v", u'actual', u'howev', u'alon', u'unfortun', u'perhap', u'place', u'outsid', u'mani', u'chang', u"here'", u'ourselv', u'onc', u'unlik', u'alreadi', u'formerli', u'respect', u'mere', u'yourselv', u'quit', u'differ', u'describ', u'wai', u"there'", u'hardli', u'sometim', u'sinc', u'everybodi', u'besid', u'immedi', u'dure', u'especi', u'regard', u"c'", u'specifi', u'ani', u"what'", u'whereaft', u'togeth', u'mostli', u'latterli', u'herebi', u'whenev', u'provid', u"he'", u'obvious', u'whatev', u'abov', u'otherwis', u"who'", u'everyon', u'sai', u'give', u'thereaft', u"where'", u'henc', u'anywher', u'indic', u'ar', u'sever', u'probabl', u'everywher', u'alwai', u'cours', u'welcom', u'anoth', u'moreov', u'mayb', u'anyon', u'thoroughli', u'selv', u'noth', u"a'", u'normal', u'wherea', u'someon', u'befor', u'okai', u'consid', u'mai', u'wherev', u'associ', u'avail', u'hereaft', u'appropri', u'furthermor', u'seriou', u'thu', u'correspond', u'greet', u'doe', u'caus', u'meanwhil', u'exampl', u'ignor', u'wherebi', u'thi', u'everyth', u'anybodi', u'usual'])  # stemmed version of the above list

  def __init__(self, stopwords=None, additional_stopwords=set()):
    super(StopwordNormalizer, self).__init__(applies_to=[taggers.get_tagger('non-whitespace').ID, taggers.get_tagger('hyphenated-words').ID, taggers.get_tagger('clitics').ID])

    if stopwords is not None: self.STOPWORDS_ = stopwords
    self.STOPWORDS_.update(additional_stopwords)
  #end def

  def normalize_token(self, tagger_id, token):
    if token in self.STOPWORDS_: return (tagger_id, u'')

    return (tagger_id, token)
  #end def
#end class


@register_normalizer
class UnicodeNormalizer(TokenNormalizer):
  """Normalizes tokens using the :mod:`unidecode` package."""

  NAME = 'unicode'
  ID = 24

  def normalize_token(self, tagger_id, tok):
    return (tagger_id, unidecode.unidecode(tok))
  #end def
#end class
