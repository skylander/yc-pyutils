"""
****************
*taggers* module
****************

Taggers are the first stage of tokenization. They make use of :class:`ycutils.nlp.tokenizer.taggers.Tagger` objects which contain the :meth:`ycutils.nlp.tokenizer.taggers.tag`. See :class:`ycutils.nlp.tokenizer.taggers.Tagger` for more details.

This module implements two useful base taggers: :class:`ycutils.nlp.tokenizer.taggers.Tagger` and :class:`ycutils.nlp.tokenizer.taggers.RegexTagger`.

Base taggers
============
.. autoclass:: ycutils.nlp.tokenizer.taggers.Tagger
  :members:
.. autoclass:: ycutils.nlp.tokenizer.taggers.RegexTagger
  :members:

Implemented taggers
===================
.. autoclass:: ycutils.nlp.tokenizer.taggers.NonWhitespaceTagger
  :members: NAME
  :undoc-members:

.. autoclass:: ycutils.nlp.tokenizer.taggers.HyphenatedWordsTagger
  :members: NAME
  :undoc-members:

.. autoclass:: ycutils.nlp.tokenizer.taggers.CliticsTagger
  :members: NAME
  :undoc-members:

.. autoclass:: ycutils.nlp.tokenizer.taggers.NumberTagger
  :members: NAME
  :undoc-members:

.. autoclass:: ycutils.nlp.tokenizer.taggers.PunctuationTagger
  :members: NAME
  :undoc-members:

.. autoclass:: ycutils.nlp.tokenizer.taggers.AcronymTagger
  :members: NAME
  :undoc-members:

.. autoclass:: ycutils.nlp.tokenizer.taggers.EmailTagger
  :members: NAME
  :undoc-members:

.. autoclass:: ycutils.nlp.tokenizer.taggers.URLTagger
  :members: NAME
  :undoc-members:

.. autoclass:: ycutils.nlp.tokenizer.taggers.PhoneNumberTagger
  :members: NAME
  :undoc-members:

.. autoclass:: ycutils.nlp.tokenizer.taggers.MoneyTagger
  :members: NAME
  :undoc-members:

.. autoclass:: ycutils.nlp.tokenizer.taggers.PercentageTagger
  :members: NAME
  :undoc-members:
"""

import regex as re

TAGGER_NAMES = {}
TAGGER_IDS = {}


def register_tagger(tagger):
  """
  Decorator function for registering a tagger in the tagger database. This is useful when you want to access the tagger by its name instead of instantiating it.
  """

  if tagger.NAME in TAGGER_NAMES: raise KeyError('Tagger name \'{}\' already exists.'.format(tagger.NAME))
  if tagger.ID in TAGGER_IDS: raise KeyError('Tagger ID \'{}\' already exists.'.format(tagger.ID))

  TAGGER_NAMES[tagger.NAME] = tagger
  TAGGER_IDS[tagger.ID] = tagger

  return tagger
#end def


def get_tagger(name):
  """Instantiates a :class:`ycutils.nlp.tokenizer.taggers.Tagger` object by its name."""

  return TAGGER_NAMES[name]()
#end def


class Tagger(object):
  """Tagger objects are responsible for tagging contiguous sequence of characters as a certain type."""

  NAME = 'unnamed'
  """The name of the tagger."""

  ID = 0
  """A unique ID for the tagger. Convention is to use 1-10 for basic taggers, 10-99 for everything else, and >= 100 for user-defined taggers not found in this library."""

  def __init__(self): pass

  def tag(self, sent):
    """
    Heart and soul of a tagger. Takes a sentence and returns a list of tuples `(start, end)` denoting the `start` and `end` index of the token.

    :param string sent: a string containing sentence to tag.
    :returns: list ranges that have been tagged.
    :rtype: list of tuples.
    """

    return []
  #end def
#end class


class RegexTagger(Tagger):
  """
  Convenient base :class:`ycutils.nlp.tokenizer.taggers.Tagger` that identifies certain tokens using regular expressions.

  :param re.RegexObject regex_obj: regular expression object to use for tagging.
  """

  NAME = 'regex'
  ID = 1

  def __init__(self, regex_obj):
    self.re_ = regex_obj
  #end def

  def tag(self, sent):
    for m in self.re_.finditer(sent): yield (m.start(0), m.end(0))
  #end def
#end class


@register_tagger
class NonWhitespaceTagger(RegexTagger):
  """Tagger that identifies any continguous sequence of characters (that contain no whitespace) as tokens."""

  NAME = 'non-whitespace'
  ID = 2

  def __init__(self): super(NonWhitespaceTagger, self).__init__(re.compile(ur'[^\s\p{Z}\p{C}]+', re.U | re.M))
#end class


@register_tagger
class HyphenatedWordsTagger(RegexTagger):
  """Tagger that identifies any hyphenated words."""

  NAME = 'hyphenated-words'
  ID = 10

  def __init__(self): super(HyphenatedWordsTagger, self).__init__(re.compile(ur'[a-zA-Z]\w*[\p{Pd}\p{Pc}]\s*[a-zA-Z]\w*', re.U | re.M))
#end class


@register_tagger
class CliticsTagger(RegexTagger):
  """Tagger that identifies any words containing clitics."""

  NAME = 'clitics'
  ID = 11

  def __init__(self): super(CliticsTagger, self).__init__(re.compile(ur'\S+([\'\u2019](re|ll|m|ve|s|d)|s[\'\u2019]|n[\'\u2019]t)(?=\W)', re.U | re.I))
#end class


@register_tagger
class NumberTagger(RegexTagger):
  """Tagger for recognizing numbers."""

  NAME = 'number'
  ID = 12

  def __init__(self): super(NumberTagger, self).__init__(re.compile(ur'(?<=\W)(\-\s*)?(\d[\d\,\.\s]*\d|\d)(?=\W)', re.U))
#end class


@register_tagger
class PunctuationTagger(RegexTagger):
  """Tagger for recognizing numbers."""

  NAME = 'punctuation'
  ID = 13

  def __init__(self): super(PunctuationTagger, self).__init__(re.compile(ur'[\p{P}]+', re.U))
#end class


@register_tagger
class AcronymTagger(RegexTagger):
  """Tagger for acronyms and initialisms (acronyms are defined as sequences of characters punctuated with a period, e.g. U.S. or N.A.S.A)."""

  NAME = 'acronym'
  ID = 20

  def __init__(self): super(AcronymTagger, self).__init__(re.compile(ur'\b([a-zA-Z]\.)+[a-zA-Z]\.?\b', re.U))
#end class


@register_tagger
class EmailTagger(RegexTagger):
  """Tagger for recognizing email addresses."""

  NAME = 'email'
  ID = 21

  def __init__(self): super(EmailTagger, self).__init__(re.compile(ur'[a-z][\w\_\.-]*\@[0-9a-z][\w\_\.-]*\.[a-z]{2,4}', re.I | re.U))
#end class


@register_tagger
class URLTagger(RegexTagger):
  """Tagger for recognizing URL addresses."""

  NAME = 'url'
  ID = 22

  def __init__(self): super(URLTagger, self).__init__(re.compile(ur'(((http|https|ftp)\://)[a-z0-9\-\.]+\.[a-z]{2,3}(:[a-z0-9]*)?/?([a-z0-9\-\._\?\,\'/\\\+\&\%\$#\=~])*)', re.U | re.I))
#end class


@register_tagger
class PhoneNumberTagger(RegexTagger):
  """Tagger for recognizing phone numbers."""

  NAME = 'phone-number'
  ID = 23

  def __init__(self): super(PhoneNumberTagger, self).__init__(re.compile(ur'(\+\d+\s*)?(\(\d+\)\s*)?\d+[\s\.\-][\d\-\s\.]+\d', re.U))
#end class


@register_tagger
class MoneyTagger(RegexTagger):
  """Tagger for recognizing money."""

  NAME = 'money'
  ID = 24

  def __init__(self): super(MoneyTagger, self).__init__(re.compile(ur'[\p{Sc}\$]\s*[\d\,\.]*[kmb]?', re.U | re.I))
#end class


@register_tagger
class PercentageTagger(RegexTagger):
  """Tagger for recognizing percentages."""

  NAME = 'percentage'
  ID = 25

  def __init__(self): super(PercentageTagger, self).__init__(re.compile(ur'[\d\,\.\s]+(\%|percent)', re.U))
#end class
