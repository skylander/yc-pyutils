"""
****************
*strings* module
****************

This module contains functions for manipulating strings.

Methods
=======
.. autofunction:: safe_concat
"""

import regex as re


def safe_concat(*s):
  """Safely concatenate strings, even if they are `None`.

  :param string s: variable argument list of strings to concatenate.
  :returns: a concatenated string.
  :rtype: string"""

  concat_string = ''
  for x in s:
    if x: concat_string += x

  return concat_string
#end def

RE_LSTRIP_WHITESPACES = re.compile(ur'^[\s\p{Z}\p{C}]+', re.U | re.M)
RE_RSTRIP_WHITESPACES = re.compile(ur'[\s\p{Z}\p{C}]+$', re.U | re.M)


def unicode_strip(s, lstrip=True, rstrip=True):
  """Safely strip newlines and spaces (including unicode versions of newlines and non-breaking spaces). This is done using the :mod:`regex` module.

  :param string s: string to strip of newlines and spaces.
  :param boolean lstrip: strip left trailing whitespaces.
  :param boolean rstrip: strip right trailing whitespaces.
  :returns: a string without trailing newlines/spaces.
  :rtype: string
  """

  stripped_s = s if s is not None else ''
  if lstrip: stripped_s = RE_LSTRIP_WHITESPACES.sub('', stripped_s)
  if rstrip: stripped_s = RE_RSTRIP_WHITESPACES.sub('', stripped_s)

  return stripped_s
#end def
