"""
Methods
=======

This module contains basic methods that deal with URL addresses.

.. autofunction:: sort_key
.. autofunction:: follow_url

Default settings
================
.. autodata:: USER_AGENT
"""

import urllib, urlparse, re

USER_AGENT = 'Mozilla/5.0 (X11; U; Linux i686) Gecko/20071127 Firefox/2.0.0.11'
"""The default user agent string that we will identify as. This will be used for all the downloading clients in our library."""

def sort_key(url):
  """Used in Python ``sort`` functions as a key for URLs. Giving preference to URLs from the same domains, followed by paths.

  For example::

    print ycutils.url.sort_key('http://skylander.bitbucket.org/yc-pyutils/index.html')

  will give::

    ['org', 'bitbucket', 'skylander', 'http', '/yc-pyutils/index.html', '']

  :param url: URL to get the key of.
  :returns: a list of URL components, starting with the parts of the address in reverse."""

  parsed = urlparse.urlparse(url)

  netloc_parts = parsed.netloc.split('.')
  netloc_parts.reverse()
  netloc_parts += [parsed.scheme, parsed.path, parsed.query]
  return netloc_parts
#end def

def follow_url(start_url, href):
  """Given a current URL, figures out the full path of a relative :attr:`href`.

  :param start_url: the current URL.
  :param href: the given relative URL.
  :returns: the final url, which may/may not be relative to the original URL.
  """

  parsed_result = urlparse.urlparse(start_url)
  if href.startswith('/'): return urlparse.urlunparse((parsed_result.scheme, parsed_result.netloc, href, parsed_result.params, '', ''))
  elif href.startswith('#'): return urlparse.urlunparse((parsed_result.scheme, parsed_result.netloc, parsed_result.path, parsed_result.params, parsed_result.query, href[1:]))
  elif href.startswith('http'): return href
  elif href.startswith('?'): return urlparse.urlunparse((parsed_result.scheme, parsed_result.netloc, parsed_result.path, parsed_result.params, href, ''))

  new_path = '/'.join(parsed_result.path.split('/')[:-1]) + '/' + href
  return urlparse.urlunparse((parsed_result.scheme, parsed_result.netloc, new_path, parsed_result.params, '', ''))
#end def

