"""
********************
*googlebooks* module
********************

This module contain methods for downloading descriptions of books from `Google books <http://books.google.com>`_.
This module directly connects to `Google books API <https://developers.google.com/books/>`_ using :mod:`urllib2`.

Methods
=======
.. autofunction:: download
.. autofunction:: book_id
"""
import re, datetime, urlparse, urllib, urllib2
import json, bs4

__book_opener = urllib2.build_opener()
__book_opener.addheaders = [('User-agent', 'ark-issues/0.1 (http://www.yanchuan.sg; yanchuan@cmu.edu)')]

def download(url):
  """
  Downloads the description for a `Google book <http://books.google.com>`_ given the URL.

  :param url: Google books URL.
  :returns: ``dict`` object containing information about the download. See :meth:`ycutils.urls.webpages.download`.
  """

  id = book_id(url)
  if not id: return {'success': False, 'error': 'googlebooks', 'code': '', 'message': 'Google Books URL is not valid.'}

  book_api = 'https://www.googleapis.com/books/v1/volumes/{}?projection=lite'.format(id)
  try:
    r = __book_opener.open(book_api)
  except urllib2.URLError as ue: return {'success': False, 'error': 'googlebooks', 'code': '', 'message': ue.reason}
  if not r: return {'success': False, 'error': 'wikipedia', 'code': '', 'message': 'urllib returned None.'}

  book_obj = json.load(r)
  volinfo = book_obj['volumeInfo']
  soup = bs4.BeautifulSoup(volinfo.get('description', ''), 'lxml')
  content = u'{}: {}\nBy {}\n\n{}'.format(volinfo['title'], volinfo.get('subtitle', ''), ', '.join(volinfo['authors']), soup.get_text())

  return {'success': True, 'content': content, 'mime_type': 'text/plain', 'date': volinfo['publishedDate']}
#end def

def book_id(url):
  """Extract the ID of a Google book from its URL.

  :param url: Google books URL.
  :returns: the book ID or ``None`` if not a valid URL."""

  parsed = urlparse.urlparse(url)
  if not parsed.netloc.startswith('books.google'): return None

  qs = urlparse.parse_qs(parsed.query)
  if 'id' in qs: return qs['id'][0]

  return None
#end def

