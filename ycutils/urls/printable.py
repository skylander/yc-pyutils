"""
*******************
*printable* module
*******************

Most webpages have a "print version" link which displays the content in a printer friendly version. This module contain methods for identifying that link and downloading printable versions of the webpage.

.. note:: This module relies on the logistic regression implementation in `scikit-learn <http://http://scikit-learn.org/>`_. Hence, you will need to have :mod:`sklearn` in your ``sys.path``.

.. note:: The scripts and data necessary for training the logistic regression classifier is contained in ``./printable_url`` folder of the source tree. The default model is trained on all 2361 instances of our data, achieving 98% cross validation accuracy. Feel free to modify the code as you wish to add features that you think might improve the classification accuracy.

*PrintableURL* class
====================
.. autoclass:: ycutils.urls.printable.PrintableURL
   :members:
"""

import urllib, urlparse, re, subprocess, cStringIO, tempfile, os, gzip, zlib, sys, cPickle, collections
import bs4
import numpy as np
import sklearn.linear_model, sklearn.preprocessing
import ycutils.urls, ycutils.urls.webpages

class PrintableURL:
  """
  This class implements method for identifying links that lead to a printable version of the web page, using a `logistic regression <http://en.wikipedia.org/wiki/Logistic_regression>`_ classifier.

  :param model_file: path to the logistic regression model. Defaults to ``printable.model`` in the same directory as the script.

  In a nutshell, it searches for all anchor tags (<a>) that contain the word ``print`` and runs it through our logistic regression classifier to determine the most probable printable URL. See :mod:`ycutils.urls.printable` for details on training and experiment results.

  """

  __RE_PRINT = re.compile(r'.*\bprint\b.*', re.I)
  """This regular expression determines what can be considered as a link for the classifier."""

  __classifier = None

  def __init__(self, model_file=None):
    if not model_file: model_file = os.path.join(os.path.dirname(__file__ ), 'printable.model')

    self.__classifier = {}
    with open(model_file) as f:
      self.__classifier['scaler'] = cPickle.load(f)
      self.__classifier['model'] = cPickle.load(f)
      self.__classifier['feature_names'] = cPickle.load(f)
      self.__classifier['feature_index'] = {}
      for i, feat_name in enumerate(self.__classifier['feature_names']): self.__classifier['feature_index'][feat_name] = i
    #end with
  #end def

  def find(self, url, content=None, use_rules=True):
    """
    Given the URL, finds the printable URL from the website.

    :param url: URL to find printable version of.
    :param content: the content of the URL. Defaults to ``None`` meaning we will download it using :meth:`ycutils.urls.webpages.download`.
    :param use_rules: whether to use pre-defined rules defined in :meth:`use_rules`.
    """

    if use_rules:
      printable_url = PrintableURL.use_rules(url)
      if printable_url != url: return printable_url
    #end if

    if not content:
      download_info = ycutils.urls.webpages.download(url)
      if not download_info['success']: return url

      content = download_info['content']

      if download_info['mime_type'] != 'text/html': return url
    #end if

    return self.__find_from_content(url, content)
  #end def

  def __find_from_content(self, url, content):
    soup = bs4.BeautifulSoup(content, "lxml")
    p = len(self.__classifier['feature_names'])

    a_tags = soup.find_all(self.__is_print_link)
    if not a_tags: return url

    N = len(a_tags)
    a_matrix = np.zeros((N, p), dtype=np.float)
    for i, a_tag in enumerate(a_tags):
      features = self.__extract_features(url, a_tag)
      for feat_name, feat_val in features.iteritems():
        if feat_name in self.__classifier['feature_index']:
          a_matrix[i, self.__classifier['feature_index'][feat_name]] = feat_val
    #end for
    prob = self.__classifier['model'].predict_proba(self.__classifier['scaler'].transform(a_matrix))

    i = np.argmax(prob[:, 1])
    if prob[i, 1] > 0.5: return ycutils.urls.follow_url(url, a_tags[i]['href'])

    return url
  #end def

  def __unigram_features(self, prefix, s):
    feats = collections.Counter()

    if not s: return feats
    if isinstance(s, list): s = ''.join(s)

    toks = re.split(r'[\W\s]+', s.lower())

    for t in toks:
      if not t: continue

      feats[prefix + t] = 1
    #end for

    return feats
  #end def

  def __extract_features(self, url, a_tag):
    text_features = self.__unigram_features('text_', a_tag.get_text())
    url_features = self.__unigram_features('url_', url)
    href_features = self.__unigram_features('href_', a_tag['href'])

    followed_url = ycutils.urls.follow_url(url, a_tag['href'])
    url_toks = set(filter(lambda x: x, re.split(r'[\W\s0-9]+', url.lower())))
    followed_toks = set(filter(lambda x: x, re.split(r'[\W\s0-9]+', followed_url.lower())))
    diff_toks = url_toks ^ followed_toks

    features = collections.Counter()
    features += url_features
    features += href_features
    features += self.__unigram_features('title_', a_tag.get('title'))
    features += self.__unigram_features('rel_', a_tag.get('rel'))
    features += self.__unigram_features('class_', a_tag.get('class'))
    features += self.__unigram_features('id_', a_tag.get('id'))
    features += text_features
    features += collections.Counter(['diff_' + t for t in diff_toks])
    features['meta_textlen'] = sum(text_features.values())
    features['meta_hreflen'] = sum(href_features.values())
    features['meta_difflen'] = len(diff_toks)

    return features
  #end def

  def __is_print_link(self, tag):
    if tag.name != 'a': return False

    href = tag.get('href')

    if not href: return False
    if tag.get('href').startswith('javascript'): return False
    if tag.get('href').startswith('#'): return False

    if self.__RE_PRINT.match(str(tag)): return True

    return False
  #end def

  @staticmethod
  def use_rules(url):
    """
    Applies our rules to find the printable URL.

    :param url: URL to find printable version of.
    :returns: the printable URL if found or the original :attr:`url` if no rule is found to apply.

    .. note:: The list of websites with rules are (`working as of 11/02/2012`): `New York Times <http://www.nytimes.com>`_, `Bloomberg News <http://www.bloomberg.com>`_, `ABC News <http://www.abcnews.com>`_, `Reuters <http://www.reuters.com>`_, `The Economist <http://www.economist.com>`_, `Forbes <http://www.forbes.com/>`_, `Foreign Policy <http://www.foreignpolicy.com>`_, `Fox News <http://www.foxnews.com>`_, `ESPN <http://espn.go.com>`_, `Huffington Post <http://www.huffingtonpost.com>`_, `Los Angeles Times <http://www.latimes.com>`_, `The New Yorker <http://www.newyorker.com>`_, `Real Clear Politics <http://www.realclearpolitics.com>`_, `British Broadcasting Corporation <http://www.bbc.co.uk>`_, `The Dailymail <http://www.dailymail.co.uk>`_, `The Guardian <http://www.guardian.co.uk>`_, `The Independent <http://www.independent.co.uk>`_, `National Aeronautics and Space Administration <http://www.nasa.gov>`_.

    .. note:: This is a `static` method.
    """

    parsed_result = urlparse.urlparse(url)

    qs = urlparse.parse_qs(parsed_result.query)
    path_string = parsed_result.path

    modified = False

    ### NYTIMES.COM ###
    if 'nytimes.com' in parsed_result.netloc:
      qs['pagewanted'] = 'print'
      qs['_r'] = '0'
      modified = True
    #end if

    ### ABCNEWS.GO.COM ###
    if 'abcnews.go.com' in parsed_result.netloc:
      if path_string.endswith('story'):
        path_string = path_string.replace('/story', '/print')
      elif path_string.endswith('Story'):
        path_string = path_string.replace('/Story', '/print')
      else:
        qs.pop('page', '')
        qs['singlePage'] = 'true'
      #end if
      modified = True
    #end if

    ### REUTERS.COM ###
    if 'reuters.com' in parsed_result.netloc:
      m = re.match(r'.*[/\-]id([A-Z0-9]+)$', parsed_result.path)

      if m:
        key = None

        first_subdomain = parsed_result.netloc.split('.')[0]

        if first_subdomain in ['www', 'uk', 'in']: # http://www.reuters.com/assets/print?aid=USTRE6541JD20100605
          path_string = '/assets/print'
          key = 'aid'
        elif first_subdomain in ['af', 'ca']: # http://af.reuters.com/articlePrint?articleId=AFL832340520091008
          path_string = '/articlePrint'
          key = 'articleId'
        #end if

        if key:
          qs.clear()
          qs[key] = m.group(1)
          modified = True
        #end if
      #end if
    #end if

    ### BLOOMBERG.COM ###
    if 'bloomberg.com' in parsed_result.netloc:
      if path_string.startswith('/news/'):
        path_string = re.sub(r'^\/news\/', '/news/print/', path_string)
        modified = True
      elif path_string.startswith('/apps/news') and 'sid' in qs:
        qs['pid'] = '21070001'
        modified = True
      #end if
    #end if

    ### ECONOMIST.COM ###
    if 'www.economist.com' in parsed_result.netloc:
      path_string = path_string + '/print'
      qs.clear()
      modified = True
    #end if

    ### FORBES.COM ###
    if 'forbes.com' in parsed_result.netloc:
      path_string = path_string.replace('home/free_forbes/', 'forbes/')
      modified = True

      if path_string.endswith('.html'): path_string = re.sub(r'\.html$', '_print.html', path_string)
      elif path_string.endswith('/'): path_string = path_string + 'print/'
      else: modified = False
    #end if

    ### FOREIGNPOLICY.COM ###
    if 'www.foreignpolicy.com' in parsed_result.netloc:
      qs.clear()
      qs['print'] = 'yes'
      qs['hidecomments'] = 'yes'
      qs['page'] = 'full'

      modified = True
    #end if

    ### FOXNEWS.COM ###
    if 'www.foxnews.com' in parsed_result.netloc:
      if path_string.endswith('/'):
        path_string = path_string + 'print'
        modified = True
      #end if
    #end if

    ### ESPN.GO.COM ###
    if 'espn.go.com' in parsed_result.netloc:
      path_string = 'espn/print'
      modified = True
    #end if

    ### HUFFINGTONPOST.GO.COM ###
    if 'huffingtonpost.com' in parsed_result.netloc:
      qs['view'] = 'print'
      modified = True
    #end if

    ### ARTICLES.LATIMES.COM ###
    if 'articles.latimes.com' in parsed_result.netloc:
      path_string = '/print' + path_string
      modified = True
    #end if

    ### NEWYORKER.cOM ###
    if 'newyorker.com' in parsed_result.netloc:
      qs['printable'] = 'true'
      modified = True
    #end if

    ### REALCLEARPOLITICS.COM ###
    if 'realclearpolitics.com' in parsed_result.netloc:
      path_string = '/printpage/'
      qs['url'] = url
      modified = True
    #end if

    ### WWW.BBC.CO.UK ###
    if 'www.bbc.co.uk' in parsed_result.netloc:
      qs['print'] = 'true'
      modified = True
    #end if

    ### DAILYMAIL.CO.UK ###
    if 'dailymail.co.uk' in parsed_result.netloc:
      qs['printingPage'] = 'true'
      modified = True
    #end if

    ### GUARDIAN.CO.UK ###
    if 'guardian.co.uk' in parsed_result.netloc:
      path_string = path_string + '/print'
      modified = True
    #end if

    ### INDEPENDENT.CO.UK ###
    if 'independent.co.uk' in parsed_result.netloc:
      qs['printService'] = 'print'
      modified = True
    #end if

    ### NASA.GOV ###
    if 'nasa.gov' in parsed_result.netloc:
      (root, ext) = os.path.splitext(path_string)
      path_string = root + '_prt.htm'
      modified = True
    #end if

    if not modified: return url

    qs_string = urllib.urlencode(qs, doseq=True)

    return urlparse.urlunparse((parsed_result.scheme, parsed_result.netloc, path_string, parsed_result.params, qs_string, parsed_result.fragment))
  #end def
#end class

