"""
*****************
*webpages* module
*****************

This module contain methods for downloading webpages.

Methods
=======
This module contains basic methods that deals with downloading HTML webpages.
It requires `GNU Wget <http://www.gnu.org/software/wget/>`_ (version >= 1.13) to be installed on the system.
Also, it uses `Beautiful Soup <http://www.crummy.com/software/BeautifulSoup/>`_ for parsing the websites.

.. autofunction:: download

Default settings
================
.. autodata:: WGET_PATH
.. autodata:: MAX_TRIES
"""

import urllib, urlparse, re, subprocess, cStringIO, tempfile, os, gzip, zlib, datetime, sys, cPickle, collections, codecs
import ycutils.urls
import bs4

WGET_PATH = '/home/ysim/tools/wget-1.13.4/bin/wget'
"""Finds the path to your system installation of Wget."""

MAX_TRIES = 3
"""Maximum number of tries to download each URL."""

USER_AGENT = ycutils.urls.USER_AGENT

def download(url, referer=None, wget_path=WGET_PATH, user_agent=ycutils.urls.USER_AGENT, max_tries=MAX_TRIES, save_to=None):
  """
  Downloads the given URL by passing it through `Wget <http://www.gnu.org/software/wget/>`_.

  :param url: URL of website to download.
  :param referer: referer URL to announce to web server. By default, it is the same as :attr:`url`.
  :param wget_path: path to Wget executable. Defaults to Wget on your system path (see :attr:`WGET_PATH`).
  :param user_agent: user agent string to identify yourself as. Defaults to :attr:`ycutils.urls.USER_AGENT`.
  :param max_tries: number of times for Wget to retry. Defaults to :attr:`MAX_TRIES` setting.
  :param save_to: location of the file to save to. Defaults to the temporary directory and file will be removed after downloading.
  :returns: ``dict`` object containing information about the download. 
  
  The ``dict`` will contain the following keys if the download was a success:

  * ``success``: ``True``.
  * ``date``: Web server declared updated date of URL.
  * ``content``: Content at URL (could be binary).
  * ``mime_type``: MIME type of downloaded content.

  If the download failed, it will contain the following:

  * ``success``: ``False``.
  * ``error``: System that caused the error (could be ``wget`` or ``http``).
  * ``code``: Error return code.
  * ``message``: Descriptive, human-readable (hopefully!) message about the error.

  .. note:: If the server sends back a GZip compressed file, we will attempt to deflate it using the Python :mod:`gzip` and :mod:`zlib` libraries.
  """

  if save_to: save_path = save_to
  else: save_path = tempfile.NamedTemporaryFile(delete=False).name

  wget_command = [wget_path, url, '--server-response', '--quiet', '--output-document', save_path, '--user-agent', user_agent, '--referer', referer if referer else url, '--no-check-certificate', '--tries', str(max_tries)]
  p = subprocess.Popen(wget_command, shell=False, stdout=subprocess.PIPE, stderr=subprocess.PIPE, close_fds=True)
  p.wait()
  return_code = p.returncode

  http_headers = {}
  http_code = -1
  http_message = 'no message'
  for line in p.stderr:
    line = line.strip()
    if line.startswith('HTTP/'):
      m = re.match('^HTTP/\d\.\d (\d\d\d) (.*)', line)

      if m:
        http_code = m.group(1)
        http_message = m.group(2)
      #end if
      continue
    #end if

    if ':' not in line: continue
    (key, value) = line.split(':', 1)
    http_headers[key.strip().lower()] = value.strip()
  #end for

  if return_code != 0 and return_code != 8: # 0 for ok, 8 for http errors, the rest are pretty much unrecoverable
    if os.path.exists(save_path): os.unlink(save_path)
    return {'success': False, 'error': 'wget', 'code': return_code, 'message': ''}
  #end if

  if return_code == 8:
    if os.path.exists(save_path): os.unlink(save_path)
    return {'success': False, 'error': 'http', 'code': http_code, 'message': http_message}
  #end if

  content = None
  if return_code == 0 and os.path.exists(save_path):
    with open(save_path, 'rb') as f: content = f.read()
    try:
      if not save_to: os.unlink(save_path)
    except OSError: pass

    del save_path

    content_encoding = http_headers.get('content-encoding', None)
    if content_encoding:
      try:
        if content_encoding == 'deflate':
          uncompressed = zlib.decompress(content)
        elif content_encoding in ['gzip', 'x-gzip']:
          uncompressed = gzip.GzipFile('', 'rb', 9, cStringIO.StringIO(content)).read()

        content = uncompressed
      except IOError: pass # not a gzip file
      # sometimes the encoding bluff people one
    #end if
  #end if

  mime_type = http_headers.get('content-type', 'text/plain') # defaults to plain text mime type
  if ';' in mime_type: mime_type = mime_type.split(';')[0] # get the first available mime type

  modified_date = http_headers.get('last-modified', http_headers.get('date', datetime.datetime.utcnow().strftime('%a, %d %b %Y %H:%M:%S GMT')))

  if return_code == 0: 
    try:
      if mime_type.startswith('text'): content = content.decode('utf-8')
    except UnicodeDecodeError:
      content = content.decode('latin-1')
    return {'content': content, 'date': modified_date, 'mime_type': mime_type, 'success': True}
#end def

