"""
*******************
*wikipedia* module
*******************

This module contain methods for downloading `Wikipedia <http://www.wikipedia.org>`_ articles.
This module directly connects to `Wikipedia API <http://www.mediawiki.org/wiki/API:Main_page>`_ using :mod:`urllib2`

Module methods
==============
.. autofunction:: download
.. autofunction:: domain_and_title
"""
import re, datetime, urlparse, urllib, urllib2
import ycutils.urls
from lxml import etree
import bs4

__wiki_opener = urllib2.build_opener()
__wiki_opener.addheaders = [('User-agent', ycutils.urls.USER_AGENT)]

def download(url):
  """
  Download the `Wikipedia <http://www.wikipedia.org>`_ article text given the URL. It will use the correct language version of the Wikipedia.

  :param url: Wikipedia article URL.
  :returns: ``dict`` object containing information about the download. See :meth:`ycutils.urls.webpages.download`.
  """
  
  wiki_domain, title = domain_and_title(url)
  if not wiki_domain: return {'success': False, 'error': 'wikipedia', 'code': '', 'message': 'Bad Wikipedia URL.'}

  wiki_api = urlparse.urlunparse(('http', wiki_domain, '/w/api.php', '', urllib.urlencode({'action': 'query', 'prop': 'revisions', 'format': 'xml', 'rvprop': 'timestamp|content', 'rvlimit': 1, 'titles': title, 'rvparse': 1, 'redirects': 1}), ''))

  r = None
  try:
    r = __wiki_opener.open(wiki_api)
  except urllib2.URLError as ue: return {'success': False, 'error': 'wikipedia', 'code': '', 'message': ue.reason}

  if not r: return {'success': False, 'error': 'wikipedia', 'code': '', 'message': 'urllib returned None.'}

  el_rev = etree.parse(r).getroot().find('.//rev')
  if el_rev is None: return {'success': False, 'error': 'wikipedia', 'code': '', 'message': 'Article not found.'}

  last_edit_date = el_rev.get('timestamp')
  if not el_rev.text: return {'success': False, 'error': 'wikipedia', 'code': '', 'message': 'Article not found.'}

  el_content = bs4.BeautifulSoup(el_rev.text, 'lxml')
  return {'success': True, 'content': el_content.text, 'mime_type': 'text/plain', 'date': last_edit_date}
#end def

def domain_and_title(url, check=False):
  """
  Extracts the Wikipedia domain (language) and article title given the URL.

  :param url: Wikipedia URL.
  :param check: determines if this function call is to check the integrity of the URL, or to extract the domain and title information. Defaults to ``False``.
  :returns: if :attr:`check` is ``True``, returns a single boolean ``True`` if the URL is a valid Wikipedia article URL and ``False`` otherwise. If :attr:`check` is ``False``, returns a 2-tuple containing the Wikipedia `domain` and article `title`.
  """
  
  parsed = urlparse.urlparse(url)

  if 'wikipedia' not in parsed.netloc: return False if check else (None, None)

  qs = urlparse.parse_qs(parsed.query)
  if 'title' in qs: return True if check else (parsed.netloc, qs['title'])

  m = re.match(r'/wiki/(.+)', parsed.path)
  if m:
    title = m.group(1)
    if title.startswith('File:') or title.startswith('Image:'): return False if check else (None, None)
    return True if check else parsed.netloc, urllib.unquote_plus(title)
  #end if

  return False if check else (None, None)
#end def

