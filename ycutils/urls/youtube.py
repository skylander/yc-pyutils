"""
*******************
*youtube* module
*******************

This module contain methods for downloading `Youtube <http://www.youtube.com>`_ video descriptions.
It uses the `gdata-python-client <https://code.google.com/p/gdata-python-client/>`_ library for accessing Youtube.
See `<https://developers.google.com/youtube/code#Python>`_ for more information.

Methods
=======
.. autofunction:: download
.. autofunction:: get_video_id
"""
import re, datetime, sys
import gdata.youtube.service, gdata.service

def download(url, video_id=None):
  """
  Download the `Youtube <http://www.youtube.com>`_ video description text given the URL.

  :param url: Youtube video URL.
  :returns: ``dict`` object containing information about the download. See :meth:`ycutils.urls.webpages.download`.

  .. note:: This module uses the `Google Data APIs Python Client library <http://code.google.com/p/gdata-python-client/>`_ to retrieve information about Youtube."""

  if not video_id: video_id = get_video_id(url)
  if not video_id: return {'success': False, 'error': 'youtube', 'message': 'Bad Youtube URL.', 'code': ''}

  yt_service = gdata.youtube.service.YouTubeService()
  yt_service.ssl = True

  try:
    entry = yt_service.GetYouTubeVideoEntry(video_id=video_id)
  except gdata.service.RequestError as e:
    return {'success': False, 'error': 'youtube', 'message': e[0]['body'], 'code': e[0]['status']}
  #end try

  content = entry.media.title.text + '\n\n' + entry.media.description.text

  published_date = None
  try:
    published_date = datetime.datetime.strptime(entry.published.text, '%Y-%m-%dT%H:%M:%S.%fZ').strftime('%a, %d %b %Y %H:%M:%S GMT')
  except ValueError: pass

  return {'success': True, 'content': content, 'mime_type': 'text/plain', 'date': published_date}
#end def

def get_video_id(url):
  """Gets the Youtube video ID from the URL.

  :param url: Youtube video URL.
  :returns: The Youtube video ID."""

  m = re.match(r'http://.*youtube\.com/.*v\=([\w\-]+).*', url)
  if not m: return None

  return m.group(1)
#end def

